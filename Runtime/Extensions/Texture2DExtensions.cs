﻿using UnityEngine;

namespace Playbite.Extensions
{
  /// <summary>
  /// Extensions for texture.
  /// </summary>
  public static class Texture2DExtensions
  {
    /// <summary>
    /// Duplicates the texture.
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static Texture2D Duplicate(this Texture2D source)
    {
      RenderTexture renderTex = RenderTexture.GetTemporary(
            source.width,
            source.height,
            0,
            RenderTextureFormat.Default,
            RenderTextureReadWrite.Linear);

      Graphics.Blit(source, renderTex);
      RenderTexture previous = RenderTexture.active;
      RenderTexture.active = renderTex;
      Texture2D readableText = new Texture2D(source.width, source.height);
      readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
      readableText.Apply();
      RenderTexture.active = previous;
      RenderTexture.ReleaseTemporary(renderTex);
      return readableText;
    }
  }
}
