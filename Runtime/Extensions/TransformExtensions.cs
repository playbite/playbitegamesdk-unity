﻿using UnityEngine;

namespace Playbite.Extensions
{
	public static class TransformExtensions
	{
		/// <summary>
		/// Removes and destroys all children in a transform.
		/// </summary>
		/// <param name="transform"></param>
		public static void DestroyChildren(this Transform transform)
		{
			int count = transform.childCount;
			for (int i = count - 1; i >= 0; --i)
			{
				transform.GetChild(i).gameObject.DestroyProperly();
			}
			transform.DetachChildren();
		}

		/// <summary>
		/// Transforms a point without affecting scale.
		/// </summary>
		/// <param name="transform"></param>
		/// <param name="position"></param>
		/// <returns></returns>
		public static Vector3 TransformPointUnscaled(this Transform transform, Vector3 position)
		{
			var localToWorldMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
			return localToWorldMatrix.MultiplyPoint3x4(position);
		}
	}
}
