﻿using UnityEngine;

namespace Playbite.Extensions
{
	public static class GameObjectExtensions
	{
		/// <summary>
		/// Destroys the game object correctly depending on whether you're in edit mode or not.
		/// </summary>
		/// <param name="gameObject"></param>
		public static void DestroyProperly(this GameObject gameObject)
		{
			if (gameObject == null) return;

#if UNITY_EDITOR
			if (Application.isPlaying)
			{
				Object.Destroy(gameObject);
			}
			else
			{
				Object.DestroyImmediate(gameObject);
			}
#else
			Object.Destroy(gameObject);
#endif
		}

		/// <summary>
		/// Checks if a GameObject has been destroyed.
		/// </summary>
		/// <param name="gameObject"></param>
		/// <returns></returns>
		public static bool IsDestroyed(this GameObject gameObject)
		{
			return gameObject == null && !object.ReferenceEquals(gameObject, null);
		}
	}
}
