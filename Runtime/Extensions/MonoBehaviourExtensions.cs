﻿using System.Collections;
using UnityEngine;

namespace Playbite.Extensions
{
	/// <summary>
	/// Extensions for behaviours.
	/// </summary>
	public static class MonoBehaviourExtensions
	{
		/// <summary>
		/// Performs the action at the end of the current frame.
		/// </summary>
		/// <param name="behaviour"></param>
		/// <param name="action"></param>
		public static void PerformInEndOfFrame(this MonoBehaviour behaviour, System.Action action)
		{
			behaviour.StartCoroutine(PerformInEndOfFrame(action));
		}

		/// <summary>
		/// Performs the action after specified seconds have passed and at the end of the current frame.
		/// </summary>
		/// <param name="behaviour"></param>
		/// <param name="seconds" ></ param >
		/// <param name="action"></param>
		public static void PerformWithDelayInEndOfFrame(this MonoBehaviour behaviour, float seconds, System.Action action)
		{
			behaviour.StartCoroutine(PerformWithDelayInEndOfFrame(seconds, action));
		}

		/// <summary>
		/// Performs the action after specified seconds have passed.
		/// </summary>
		/// <param name="behaviour"></param>
		/// <param name="seconds"></param>
		/// <param name="action"></param>
		public static void PerformWithDelay(this MonoBehaviour behaviour, float seconds, System.Action action)
		{
			behaviour.StartCoroutine(PerformWithDelay(seconds, action));
		}

		/// <summary>
		/// Performs the action after specified unscaled seconds have passed.
		/// </summary>
		/// <param name="behaviour"></param>
		/// <param name="seconds"></param>
		/// <param name="action"></param>
		public static void PerformWithRealDelay(this MonoBehaviour behaviour, float seconds, System.Action action)
		{
			behaviour.StartCoroutine(PerformWithRealDelay(seconds, action));
		}

		private static bool IsActionAlive(System.Action action)
		{
			if (action == null)
			{
				return false;
			}
			if (action.Method.IsStatic)
			{
				return true;
			}
			var owner = action.Target;
			var gameObject = owner as GameObject;

			if (ReferenceEquals(gameObject, null))
			{
				var behavior = owner as MonoBehaviour;
				if (behavior != null)
				{
					gameObject = behavior.gameObject;
				}
			}

			if (!ReferenceEquals(gameObject, null))
			{
				return !gameObject.IsDestroyed();
			}

			return true;
		}

		private static IEnumerator PerformWithDelayInEndOfFrame(float seconds, System.Action action)
		{
			yield return new WaitForSeconds(seconds);
			yield return new WaitForEndOfFrame();
			if (IsActionAlive(action))
			{
				action();
			}
		}

		private static IEnumerator PerformInEndOfFrame(System.Action action)
		{
			yield return new WaitForEndOfFrame();
			if (IsActionAlive(action))
			{
				action();
			}
		}

		private static IEnumerator PerformWithDelay(float seconds, System.Action action)
		{
			yield return new WaitForSeconds(seconds);
			if (IsActionAlive(action))
			{
				action();
			}
		}

		private static IEnumerator PerformWithRealDelay(float seconds, System.Action action)
		{
			yield return new WaitForSecondsRealtime(seconds);
			if (IsActionAlive(action))
			{
				action();
			}
		}
	}
}