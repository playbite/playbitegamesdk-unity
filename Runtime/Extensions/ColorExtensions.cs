﻿using UnityEngine;

namespace Playbite.Extensions
{
  /// <summary>
  /// Extensions for Colors.
  /// </summary>
  public static class ColorExtensions
  {
    /// <summary>
    /// Returns a copy of the color with a different alpha value.
    /// </summary>
    /// <param name="color"></param>
    /// <param name="alpha"></param>
    /// <returns></returns>
    public static Color ChangeAlpha(this Color color, float alpha)
    {
      return new Color(color.r, color.g, color.b, alpha);
    }
  }
}