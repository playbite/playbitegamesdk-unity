﻿#if PLAYBITE_AVATARS
using UnityEngine;
using GLTFast.Logging;

namespace Playbite.Helpers
{
  /// <summary>
  /// A gltFast download provider that caches data.
  /// </summary>
  public class BasicLogger : ICodeLogger
  {
    public void Error(LogCode code, params string[] messages)
    {
      foreach (var message in messages)
        Debug.LogError(message);
    }

    public void Error(string message)
    {
      Debug.LogError(message);
    }

    public void Info(LogCode code, params string[] messages)
    {
      Debug.Log(messages);
    }

    public void Info(string message)
    {
      Debug.Log(message);
    }

    public void Warning(LogCode code, params string[] messages)
    {
      Debug.LogWarning(messages);
    }

    public void Warning(string message)
    {
      Debug.LogWarning(message);
    }
  }
}
#endif