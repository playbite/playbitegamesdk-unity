﻿using UnityEngine.SceneManagement;

namespace Playbite.Helpers
{
  /// <summary>
  /// Helper methods for scene management.
  /// </summary>
  public static class SceneHelper
  {
    /// <summary>
    /// Gets the scene name from the build index.
    /// </summary>
    /// <param name="buildIndex"></param>
    /// <returns></returns>
    public static string GetNameFromIndex(int buildIndex)
    {
      var path = SceneUtility.GetScenePathByBuildIndex(buildIndex);
      var slash = path.LastIndexOf('/');
      var name = path.Substring(slash + 1);
      var dot = name.LastIndexOf('.');

      return name.Substring(0, dot);
    }

    /// <summary>
    /// Gets the scene index from the name.
    /// </summary>
    /// <param name="sceneName"></param>
    /// <returns></returns>
    public static int GetIndexFromName(string sceneName)
    {
      for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
      {
        var currentSceneName = GetNameFromIndex(i);

        if (currentSceneName == sceneName)
        {
          return i;
        }
      }

      return -1;
    }
  }
}
