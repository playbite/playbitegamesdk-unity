﻿#if PLAYBITE_AVATARS
using GLTFast.Loading;
using System.Threading.Tasks;
using System;
using UnityEngine;
using System.Collections;
using System.Text;
using UnityEngine.Scripting;

namespace Playbite.Helpers
{
	/// <summary>
	/// A gltFast download provider that caches data.
	/// </summary>
	public class CachedDownloadProvider : IDownloadProvider
	{
		private readonly DefaultDownloadProvider _downloadProvider = new DefaultDownloadProvider();

    /// <inheritdoc />
    public async Task<IDownload> Request(Uri url)
    {
      var serializedFromCache = PlayerPrefs.GetString(url.ToString(), null);
      var deserializedFromCache = string.IsNullOrEmpty(serializedFromCache)
        ? null : SerializationHelper.FromJson<DownloadData>(serializedFromCache, null);

      if (deserializedFromCache?.Data == null || deserializedFromCache.Data.Length == 0)
      {
        var download = await _downloadProvider.Request(url);

        if (download.success)
        {
          try
          {
            PlayerPrefs.SetString(url.ToString(), SerializationHelper.ToJson(new DownloadData
            {
              Data = Convert.ToBase64String(download.data),
              IsBinary = download.isBinary
            }));
            PlayerPrefs.Save();
          }
          catch
          {
            // Intentional.
          }
        }

        return download;
      }
      else
      {
        var bytes = Convert.FromBase64String(deserializedFromCache.Data);
        return new ResolvedDownload
        {
          success = true,
          data = bytes,
          text = Encoding.UTF8.GetString(bytes),
          isBinary = deserializedFromCache.IsBinary
        };
      }
    }

    // <inheritdoc />
    public Task<ITextureDownload> RequestTexture(Uri url, bool nonReadable)
    {
      return _downloadProvider.RequestTexture(url, nonReadable);
    }

    public class DownloadData
    {
      [Preserve]
      public DownloadData()
      {
      }

      public string Data { get; set; }

      public bool? IsBinary { get; set; }
    }

    public class ResolvedDownload : IDownload
    {
      /// <inheritdoc />
      public bool success { get; set; }

      /// <inheritdoc />
      public string error { get; set; }

      /// <inheritdoc />
      public byte[] data { get; set; }

      /// <inheritdoc />
      public string text { get; set; }

      /// <inheritdoc />
      public bool? isBinary { get; set; }

      object IEnumerator.Current => throw new NotImplementedException();

      bool IEnumerator.MoveNext()
      {
        throw new NotImplementedException();
      }

      void IEnumerator.Reset()
      {
        throw new NotImplementedException();
      }
    }
  }
}
#endif