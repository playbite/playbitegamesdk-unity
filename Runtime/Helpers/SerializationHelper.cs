﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Playbite.Helpers
{
  /// <summary>
  /// Helper methods for serializing data to and from JSON.
  /// </summary>
  public static class SerializationHelper
  {
    /// <summary>
    /// Serializes an object to a JSON string.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="data"></param>
    /// <returns></returns>
    public static string ToJson<T>(T data)
    {
      return JsonUtility.ToJson(data);
      //return JsonConvert.SerializeObject(data);
    }

    /// <summary>
    /// Deserializes an object from a JSON string.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="data"></param>
    /// <returns></returns>
    public static T FromJson<T>(string data)
    {
      try
      {
        return JsonUtility.FromJson<T>(data);
      }
      catch (Exception err)
      {
        Debug.LogError(err);
        return default;
      }
      //return JsonConvert.DeserializeObject<T>(data);
    }

    /// <summary>
    /// Deserializes an object from a JSON string or returns a default if it can't be deserialized.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="data"></param>
    /// <param name="defaultValue"></param>
    /// <returns></returns>
    public static T FromJson<T>(string data, T defaultValue)
    {
      try
      {
        return JsonUtility.FromJson<T>(data);
        //return JsonConvert.DeserializeObject<T>(data);
      }
      catch
      {
        return defaultValue;
      }
    }

    /// <summary>
    /// Selects a value from the specified JSON using JSON Path syntax.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="data"></param>
    /// <param name="jsonPath"></param>
    /// <returns></returns>
    public static T SelectValue<T>(string data, string jsonPath)
      where T : class
    {
      var parsed = JObject.Parse(data);
      var token = parsed.SelectToken(jsonPath);

      if (token != null)
        return token.ToObject<T>();

      return default;
    }

    /// <summary>
    /// Selects values from the specified JSON using JSON Path syntax.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="data"></param>
    /// <param name="jsonPath"></param>
    /// <returns></returns>
    public static IEnumerable<T> SelectValues<T>(string data, string jsonPath)
      where T : class
    {
      var parsed = JObject.Parse(data);
      var tokens = parsed.SelectTokens(jsonPath);

      if (tokens == null)
        yield break;

      foreach (var token in tokens)
      {
        var value = token.ToObject<T>();
        if (value == null) continue;

        yield return value;
      }
    }
  }
}
