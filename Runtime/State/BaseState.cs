﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Playbite.State
{
	/// <summary>
	/// A base class for state that provides notification for changed properties.
	/// </summary>
	public abstract class BaseState : INotifyPropertyChanged
	{
		/// <summary>
		/// Event triggered when a property has changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
		/// Raises the changed event if the value changed.
		/// </summary>
		/// <typeparam name="TRet"></typeparam>
		/// <param name="backingField"></param>
		/// <param name="newValue"></param>
		/// <param name="propertyName"></param>
		/// <returns></returns>
		public TRet RaiseAndSetIfChanged<TRet>(
            ref TRet backingField,
            TRet newValue,
            [CallerMemberName] string propertyName = null)
    {
      if (EqualityComparer<TRet>.Default.Equals(backingField, newValue))
      {
        return newValue;
      }

      backingField = newValue;
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
      return newValue;
    }
  }
}
