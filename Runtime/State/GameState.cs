﻿using Playbite.Helpers;
using Playbite.Models;
using UnityEngine;

namespace Playbite.State
{
	/// <summary>
	/// The game state.
	/// </summary>
	public class GameState : BaseState
	{
		private bool _isLoading;
		/// <summary>
		/// Whether the game state is loading.
		/// </summary>
		public bool IsLoading
		{
			get => _isLoading;
			set => RaiseAndSetIfChanged(ref _isLoading, value);
		}

		private Game _game;
		/// <summary>
		/// The game.
		/// </summary>
		public Game Game
		{
			get => _game;
			set => RaiseAndSetIfChanged(ref _game, value);
		}

		private GameSettings _gameSettings;
		/// <summary>
		/// The game settings.
		/// </summary>
		public GameSettings GameSettings
		{
			get => _gameSettings;
			set => RaiseAndSetIfChanged(ref _gameSettings, value);
		}

		private string _gameData;
		/// <summary>
		/// The game data.
		/// </summary>
		public string GameData
		{
			get => _gameData;
			set => RaiseAndSetIfChanged(ref _gameData, value);
		}

		/// <summary>
		/// Parses the custom game data deserialized as an object.
		/// </summary>
		/// <returns></returns>
		public T ParseGameData<T>() where T : class
		{
			return string.IsNullOrEmpty(GameData)
				? null
				: SerializationHelper.FromJson<T>(GameData);
		}

		/// <summary>
		/// Gets a string value from the game data.
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public string GetStringValue(string path)
		{
			try
			{
				return string.IsNullOrEmpty(GameData) ? null : SerializationHelper.SelectValue<string>(GameData, path);
			}
			catch
      {
				return null;
      }
		}

		/// <summary>
		/// Gets a color value from the game data.
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public Color? GetColorValue(string path)
		{
			try
			{
				var colorStr = string.IsNullOrEmpty(GameData) ? null : SerializationHelper.SelectValue<string>(GameData, path);

				if (string.IsNullOrEmpty(colorStr) || !ColorUtility.TryParseHtmlString(colorStr, out var color))
					return null;

				return color;
			}
			catch
      {
				return null;
      }
		}
	}
}
