﻿using System;
using System.ComponentModel;
using Playbite.Models;

namespace Playbite.State
{
	/// <summary>
	/// The authentication state.
	/// </summary>
	public class AuthState : BaseState
	{
		private string _authToken;
		/// <summary>
		/// The current auth token.
		/// </summary>
		public string AuthToken
		{
			get => _authToken;
			set => RaiseAndSetIfChanged(ref _authToken, value);
		}

		private bool _isLoading;
		/// <summary>
		/// Whether the authentication is loading.
		/// </summary>
		public bool IsLoading
		{
			get => _isLoading;
			set => RaiseAndSetIfChanged(ref _isLoading, value);
		}

		private User _user;
		/// <summary>
		/// The user that is authenticated.
		/// </summary>
		public User User
		{
			get => _user;
			set => RaiseAndSetIfChanged(ref _user, value);
		}

		/// <summary>
		/// Whether the user is signed in.
		/// </summary>
		public bool IsSignedIn => User != null;
	}
}
