﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Playbite.Models;
using UnityEngine;

namespace Playbite.State
{
	/// <summary>
	/// The player state.
	/// </summary>
	public class PlayerState : BaseState
	{
		private bool _isLoading;
		/// <summary>
		/// Whether the player state is loading.
		/// </summary>
		public bool IsLoading
		{
			get => _isLoading;
			set => RaiseAndSetIfChanged(ref _isLoading, value);
		}

		private List<LoadedAsset> _loadedAssets;
		/// <summary>
		/// The current loaded asset list.
		/// </summary>
		public List<LoadedAsset> LoadedAssets
		{
			get => _loadedAssets;
			set => RaiseAndSetIfChanged(ref _loadedAssets, value);
		}

		private Asset _avatarAsset;
		/// <summary>
		/// The current avatar (if any).
		/// </summary>
		public Asset AvatarAsset
		{
			get => _avatarAsset;
			set => RaiseAndSetIfChanged(ref _avatarAsset, value);
		}

		public GameObject _avatarPrefab;
		/// <summary>
		/// The current avatar prefab (if any).
		/// </summary>
		public GameObject AvatarPrefab
		{
			get => _avatarPrefab;
			set => RaiseAndSetIfChanged(ref _avatarPrefab, value);
		}
	}
}
