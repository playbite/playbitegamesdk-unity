﻿namespace Playbite.State
{
	/// <summary>
	/// The root app state.
	/// </summary>
	public class AppState
	{
		/// <summary>
		/// The authentication state.
		/// </summary>
		public AuthState Auth { get; } = new AuthState();

		/// <summary>
		/// The game state.
		/// </summary>
		public GameState Game { get; } = new GameState();

		/// <summary>
		/// The player state.
		/// </summary>
		public PlayerState Player { get; } = new PlayerState();
	}
}
