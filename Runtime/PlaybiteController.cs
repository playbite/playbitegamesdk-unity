﻿using UnityEngine;
using Playbite.State;
using Playbite.Actions;
using Playbite.Setup;
using System;
using Playbite.Services;
using Playbite.Helpers;
using Newtonsoft.Json;
using UnityEngine.Scripting;
using Playbite.Messages;
using UnityEngine.SceneManagement;
using System.Collections;
#if PLAYBITE_NET
using Playbite.Components.Networking;
#endif

namespace Playbite
{
  /// <summary>
  /// The access point for controlling the Playbite SDK integration.
  /// </summary>
  public class PlaybiteController : MonoBehaviour
  {
    /// <summary>
    /// The current app state.
    /// </summary>
    public AppState State { get; } = new AppState();

    /// <summary>
    /// The current app state.
    /// </summary>
    public AppActions Actions { get; } = new AppActions();

    /// <summary>
    /// Provides tracking capabilities.
    /// </summary>
    public TrackingService Tracking { get; } = new TrackingService();

    /// <summary>
    /// The current room name.
    /// </summary>
    public string CurrentRoomName { get; set; } = "Lobby";

    /// <summary>
    /// The current scene name.
    /// </summary>
    public string CurrentSceneName { get; set; }

    /// <summary>
    /// Gets the singleton instance of this class.
    /// </summary>
    public static PlaybiteController Instance { get; private set; }

#if PLAYBITE_NET
    private PlaybiteRoomController _currentRoom;
    /// <summary>
    /// The current room controller.
    /// </summary>
    public PlaybiteRoomController CurrentRoom
    {
      get
      {
        if (!_currentRoom)
        {
          _currentRoom = FindObjectOfType<PlaybiteRoomController>();
        }

        return _currentRoom;
      }
    }
#endif

    /// <summary>
    /// Restarts the game (by loading the first scene).
    /// </summary>
    public void RestartGame()
    {
      SceneManager.LoadScene(0);
    }

    /// <summary>
    /// Called once before start.
    /// </summary>
    protected void Awake()
    {
#if !PLAYBITE
			return;
#endif
      if (Instance != null)
      {
        Destroy(gameObject);
        return;
      }

      Instance = this;
      DontDestroyOnLoad(this);

#if UNITY_WEBGL
      Application.targetFrameRate = -1;
#endif

      if (string.IsNullOrWhiteSpace(PlaybiteSettings.Default?.GameId))
      {
        throw new InvalidOperationException("Playbite Game Id is not configured!");
      }

      Tracking.Initialize();

      Actions?.Auth?.Refresh();
      Actions?.Game?.Refresh();
    }

#if PLAYBITE_NET && PLAYBITE_NET_FUSION
    protected void Start()
    {
      StartCoroutine(nameof(InitializeSession));

      SceneManager.activeSceneChanged += ActiveSceneChanged;
    }

    private IEnumerator InitializeSession()
    {
      var wait = new WaitForSecondsRealtime(0.2f);

      for (int i = 0; i < 100; ++i)
      {
        var gameSettings = new GameService().GetGameSettings();
        if (gameSettings != null)
        {
          if (gameSettings.GameMode == "Random" || gameSettings.GameMode == "Friends")
          {
            var launcher = new GameObject("Playbite Launcher").AddComponent<PlaybiteLauncher>();
            launcher.JoinOrCreateSession();
          }

          yield break;
        }

        yield return wait;
      }
    }

    private void ActiveSceneChanged(Scene current, Scene next)
    {
      if (next.buildIndex == 0)
      {
        StartCoroutine(nameof(InitializeSession));
      }
    }
#endif

#if UNITY_WEBGL
    /// <summary>
    /// Receives messages from the webgl JS runtime.
    /// </summary>
    /// <param name="messageStr"></param>
    public void OnMessage(string messageStr)
    {
      Debug.Log("Got message from JS: " + messageStr);

      if (string.IsNullOrWhiteSpace(messageStr)) return;

      try
      {
        var message = SerializationHelper.FromJson<PlaybiteMessage>(messageStr);

        Debug.Log("Executing action = " + message.Action);

        if (message.Action == "data-changed")
        {
          var dataChangedMessage = SerializationHelper.FromJson<PlaybiteDataChangedMessage>(messageStr);

          if (dataChangedMessage.DataType == "avatar")
          {
            Debug.Log("Refreshing player assets...");

            Actions?.Player?.RefreshAssets();
          }
          else if (dataChangedMessage.DataType == "user")
          {
            Debug.Log("Refreshing user...");

            Actions?.Auth?.Refresh();
          }
          else if (dataChangedMessage.DataType == "game-data")
          {
            Debug.Log("Refreshing game data...");

            Actions?.Game?.Refresh();
          }
          else if (dataChangedMessage.DataType == "settings")
          {
            Debug.Log("Refreshing game settings...");

            Actions?.Game?.Refresh();
          }
        }
        else if (message.Action == "mute")
        {
          Debug.Log("Muting game...");

          AudioListener.volume = 0;
        }
        else if (message.Action == "unmute")
        {
          Debug.Log("Unmuting game...");

          AudioListener.volume = 1;
        }
        else if (message.Action == "pause")
        {
          Debug.Log("Pausing game...");

          Time.timeScale = 0;
        }
        else if (message.Action == "unpause")
        {
          Debug.Log("Unpausing game...");

          Time.timeScale = 1;
        }
        else if (message.Action == "send-reaction")
        {
#if PLAYBITE_NET && PLAYBITE_NET_FUSION
          var reactionMessage = SerializationHelper.FromJson<PlaybiteReactionMessage>(messageStr);

          if (!string.IsNullOrEmpty(reactionMessage?.Emoji)
            && !string.IsNullOrEmpty(reactionMessage?.From)
            && CurrentRoom)
          {
            CurrentRoom.ReactionRpc(reactionMessage.From, reactionMessage.Emoji);
          }
#endif
        }
        else if (message.Action == "restart-game")
        {
          Debug.Log("Restarting game...");

          RestartGame();
        }
        else
        {
          Debug.Log("Action not recognized");
        }
      }
      catch (Exception err)
      {
        Debug.LogError(err);
      }
    }
#endif
  }
}
