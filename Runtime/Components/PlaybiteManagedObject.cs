﻿using Playbite.Services;
using UnityEngine;

namespace Playbite.Components
{
	/// <summary>
	/// Behavior that handles a managed object.
	/// </summary>
	public class PlaybiteManagedObject : MonoBehaviour
	{
		/// <summary>
		/// Whether to hide or show this object if this is a managed session.
		/// </summary>
		[SerializeField]
		protected bool hideIfManaged = true;

		private bool _isManaged;

		protected void Start()
		{
			_isManaged = new GameService().GetGameSettings()?.IsManaged == true;
		}

		protected void Update()
		{
			gameObject.SetActive(!_isManaged || !hideIfManaged);
		}
	}
}
