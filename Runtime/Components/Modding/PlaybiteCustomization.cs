﻿using Playbite.State;
using UnityEngine;

namespace Playbite.Components.Modding
{
  /// <summary>
  /// Playbite helper that applies a customization to an object based on game data.
  /// </summary>
  public abstract class PlaybiteCustomization : MonoBehaviour
  {
    /// <summary>
    /// The game state.
    /// </summary>
    protected GameState GameState => PlaybiteController.Instance.State?.Game;

    protected virtual void Start()
    {
      if (GameState != null)
        GameState.PropertyChanged += GameState_PropertyChanged;

      PlaybiteController.Instance.Actions.Game.Refresh();

      HandleChange();
    }

    protected void HandleChange()
    {
      if (GameState.GameData == null)
      {
        Debug.Log($"[Playbite] Game data not available.");
        return;
      }

      RefreshCustomization();
    }

    protected virtual void OnDestroy()
    {
      if (GameState != null)
        GameState.PropertyChanged -= GameState_PropertyChanged;
    }

    private void GameState_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      if (GameState.IsLoading) return;

      HandleChange();
    }

    /// <summary>
    /// Refreshes the data.
    /// </summary>
    protected abstract void RefreshCustomization();
  }
}
