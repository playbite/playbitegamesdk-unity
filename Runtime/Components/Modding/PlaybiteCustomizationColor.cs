﻿using Playbite.State;
using UnityEngine;

namespace Playbite.Components.Modding
{
	/// <summary>
	/// Playbite helper that applies a color customization to an object based on game data.
	/// </summary>
	public class PlaybiteCustomizationColor : PlaybiteCustomization
	{
		private Color _color;

		[SerializeField]
		private string _colorPath = "";
		/// <summary>
		/// Gets or sets the JSON path of the value to use.
		/// </summary>
		public string ColorPath
		{
			get => _colorPath;
			set
			{
				_colorPath = value;
				HandleChange();
			}
		}

		/// <summary>
		/// Specifies whether we want to override the customization color's alpha.
		/// </summary>
		[SerializeField]
		private bool _overrideAlpha = false;
		/// <summary>
		/// Specifies the alpha override.
		/// </summary>
		[SerializeField]
		[Range(0f, 1f)]
		private float _alpha = 1;

		/// <inheritdoc />
		protected override void RefreshCustomization()
    {
			if (ColorPath == null)
			{
				Debug.Log($"[Playbite] Value path not configured.");
				return;
			}

			var color = GameState.GetColorValue(ColorPath);

			if (color == null)
      {
				Debug.Log("[Playbite] No color found, skipping...");
				return;
      }

			if (color == _color)
      {
				Debug.Log("[Playbite] Found same color already applied, skipping...");
				return;
      }

			var renderer = GetComponentInChildren<Renderer>();

			if (!renderer || !renderer.material)
			{
				Debug.Log("[Playbite] No valid renderer or material found.");
				return;
			}

			_color = color.Value;

			if (_overrideAlpha)
			{
				color = new Color(_color.r, _color.g, _color.b, _alpha);
			}

			renderer.material.color = color.Value;
    }
	}
}
