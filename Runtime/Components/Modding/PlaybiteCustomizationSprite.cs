﻿using Playbite.Services;
using Playbite.State;
using UnityEngine;
using UnityEngine.UI;

namespace Playbite.Components.Modding
{
	/// <summary>
	/// Playbite helper that applies a sprite customization to an object based on game data.
	/// </summary>
	public class PlaybiteCustomizationSprite : PlaybiteCustomization
	{
		private string _textureUrl;

		[SerializeField]
		private string _texturePath = "";
		/// <summary>
		/// Gets or sets the JSON path of the value to use.
		/// </summary>
		public string TexturePath
		{
			get => _texturePath;
			set
			{
				_texturePath = value;
				HandleChange();
			}
		}

		/// <inheritdoc />
		protected override void RefreshCustomization()
    {
			if (string.IsNullOrEmpty(TexturePath))
			{
				Debug.Log($"[Playbite] Value path not configured.");
				return;
			}

			var textureUrl = GameState.GetStringValue(TexturePath);

			if (string.IsNullOrEmpty(textureUrl))
			{
				Debug.Log($"[Playbite] Empty or no value found for {TexturePath}");
				return;
			}

			if (textureUrl == _textureUrl)
      {
				Debug.Log("[Playbite] Found same texture URL already loaded, skipping...");
				return;
      }

			var renderer = GetComponentInChildren<SpriteRenderer>();
			var image = GetComponentInChildren<Image>();

			if ((!renderer || !renderer.material) && !image)
			{
				Debug.Log("[Playbite] No valid renderer or material found.");
				return;
			}

			Debug.Log($"[Playbite] Loading texture at URL: {textureUrl}");

			new ContentService().DownloadAndLoadTexture(textureUrl, texture =>
			{
				if (texture != null)
				{
					Debug.Log($"[Playbite] Loaded texture.");

					_textureUrl = textureUrl;

					var ppu = renderer && renderer.sprite
						? renderer.sprite.pixelsPerUnit
						: image.sprite ? image.sprite.pixelsPerUnit : 100;

					var sprite = Sprite.Create(texture,
						new Rect(0, 0, texture.width, texture.height),
						Vector2.one * 0.5f,
						ppu);

					if (image)
						image.sprite = sprite;
					else
						renderer.sprite = sprite;
				}
				else
				{
					Debug.Log($"[Playbite] Could not load texture.");
				}
			});
    }
	}
}
