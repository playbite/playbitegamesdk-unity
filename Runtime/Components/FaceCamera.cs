﻿using UnityEngine;

namespace Playbite.Components
{
	/// <summary>
	/// Behavior that always faces the camera.
	/// </summary>
	public class FaceCamera : MonoBehaviour
	{
		/// <summary>
		/// The camera to face.
		/// </summary>
		[SerializeField]
		protected Camera _camera;
		/// <summary>
		/// Whether to use the inverse direction.
		/// </summary>
		[SerializeField]
		protected bool useInverseDirection = false;

		private Transform myTransform;
		private Transform cameraTransform;

		protected void Awake()
		{
			myTransform = transform;
			cameraTransform = _camera != null ? _camera.transform : Camera.main.transform;
		}

		protected void LateUpdate()
		{
			if (cameraTransform)
			{
				myTransform.forward = cameraTransform.forward * (useInverseDirection ? 1 : -1);
			}
		}
	}
}
