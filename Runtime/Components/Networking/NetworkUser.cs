﻿#if PLAYBITE_NET && PLAYBITE_NET_FUSION
using Fusion;

namespace Playbite.Components.Networking
{
  /// <summary>
  /// A representation of a user that can be synced over the network.
  /// </summary>
  public struct NetworkUser : INetworkStruct
  {
    /// <summary>
    /// The user ID.
    /// </summary>
    public NetworkString<_32> Id;

    /// <summary>
    /// The display name.
    /// </summary>
    public NetworkString<_32> DisplayName;

    /// <summary>
    /// The image url.
    /// </summary>
    public NetworkString<_512> ImageUrl;
  }
}
#endif