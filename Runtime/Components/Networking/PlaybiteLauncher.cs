﻿#if PLAYBITE_NET && PLAYBITE_NET_FUSION
using System.Collections.Generic;
using Fusion;
using Playbite.Helpers;
using Playbite.Setup;
using Fusion.Photon.Realtime;
#endif
using System;
using UnityEngine;
using Playbite.Services;

namespace Playbite.Components.Networking
{
  /// <summary>
  /// Component that handles connecting to a room.
  /// </summary>
  public class PlaybiteLauncher : MonoBehaviour
  {
    /// <summary>
    /// The number of extra peers to run. This can be useful for testing the game.
    /// </summary>
    [Tooltip("The number of extra peers to run. This can be useful for testing the game.")]
    [SerializeField]
    private int _extraPeers = 0;

    /// <summary>
    /// An object to deactivate when the first peer is connected.
    /// </summary>
    [Tooltip("An object to deactivate when the first peer is connected.")]
    [SerializeField]
    private GameObject _objectToDeactivate;
    
    private int _peerId = 0;

    /// <summary>
    /// Called when launcher state changes. The second parameter is a description message.
    /// </summary>
    public event Action<string, string> StateChanged;

#if PLAYBITE_NET && PLAYBITE_NET_FUSION
    private void Awake()
    {
      Debug.Log("[Playbite] Launcher initialized");

      foreach (var runner in FindObjectsOfType<NetworkRunner>())
      {
        runner.Shutdown();
      }
    }

    /// <summary>
    /// Joins or creates a new session.
    /// </summary>
    public void JoinOrCreateSession()
    {
      JoinOrCreateSessionAsync();
    }

    private async void JoinOrCreateSessionAsync()
    {
      try
      {
        Debug.Log("[Playbite] Connecting to game session...");

        PlaybiteHooks.OnMatchmakingStarted();
        StateChanged?.Invoke("Started", "Connecting...");

        var runner = new GameObject($"Network Runner {_peerId}").AddComponent<NetworkRunner>();
        runner.IsVisible = _peerId == 0;

        var gameSettings = new GameService().GetGameSettings();
        var gameId = gameSettings?.SessionId ?? PlaybiteSettings.Default.GameId;
        var maxPlayers = gameSettings != null && gameSettings.MaxPlayers > 0
          ? gameSettings.MaxPlayers
          : PlaybiteSettings.Default.MultiplayerSettings?.MaxPlayers ?? 2;

        Debug.Log($"[Playbite] Session ID = {gameId}, Max Players = {maxPlayers}");

        var appSettings = PhotonAppSettings.Instance.AppSettings.GetCopy();

        appSettings.AppIdFusion = string.IsNullOrEmpty(gameSettings?.NetworkAppId)
          ? appSettings.AppIdFusion : gameSettings.NetworkAppId;

        appSettings.FixedRegion = gameSettings?.Region == "local"
          ? "" : (string.IsNullOrEmpty(gameSettings?.Region) ? appSettings.FixedRegion : gameSettings.Region);

        Debug.Log($"[Playbite] App ID = {appSettings.AppIdFusion}, Region = {appSettings.FixedRegion}");

        var result = await runner.StartGame(new StartGameArgs
        {
          CustomPhotonAppSettings = appSettings,
          GameMode = GameMode.Shared,
          DisableClientSessionCreation = false,
          PlayerCount = maxPlayers,
          SessionProperties = new Dictionary<string, SessionProperty>
          {
            {"GameId", gameId}
          },
          AuthValues = new AuthenticationValues
          {
            UserId = new UserService().GetMe()?.DisplayName
          },
        });

        if (result?.Ok != true)
        {
          Debug.LogError("[Playbite] Session join/create failed. " + result?.ErrorMessage);
          PlaybiteHooks.OnMatchmakingFailed(result?.ErrorMessage);
          StateChanged?.Invoke("Error", result?.ErrorMessage);
          return;
        }

        Debug.Log("[Playbite] Connected to game, loading session...");

        if (runner.IsSharedModeMasterClient || runner.IsServer)
        {
          if (!string.IsNullOrEmpty(PlaybiteSettings.Default.MultiplayerSettings?.GameScene))
          {
            var sceneIndex = SceneHelper.GetIndexFromName(PlaybiteSettings.Default.MultiplayerSettings?.GameScene);
            if (sceneIndex < 0)
            {
              Debug.LogError($"Scene '{PlaybiteSettings.Default.MultiplayerSettings?.GameScene}' could not be found!");
              return;
            }

            runner.SetActiveScene(sceneIndex);
          }
          else if (PlaybiteSettings.Default.MultiplayerSettings?.RoomPrefab)
          {
            runner.Spawn(PlaybiteSettings.Default.MultiplayerSettings?.RoomPrefab);
          }
        }

        PlaybiteHooks.OnMatchmakingComplete();
        StateChanged?.Invoke("Connected", "");
        Debug.Log("[Playbite] Session set up successfully.");

        if (runner.IsVisible && _objectToDeactivate)
        {
          _objectToDeactivate.SetActive(false);
        }
      }
      catch (Exception error)
      {
        var errorMessage = error.ToString();
        Debug.LogError("[Playbite] Session join/create failed. " + errorMessage);
        PlaybiteHooks.OnMatchmakingFailed(errorMessage);
        StateChanged?.Invoke("Error", errorMessage);
      }
    }
#endif
  }
}
