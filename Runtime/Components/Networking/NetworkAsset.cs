﻿#if PLAYBITE_NET && PLAYBITE_NET_FUSION
using Fusion;

namespace Playbite.Components.Networking
{
  /// <summary>
  /// Asset information that can be synced through the network.
  /// </summary>
  public struct NetworkAsset : INetworkStruct
  {
    /// <summary>
    /// The asset type.
    /// </summary>
    public NetworkString<_16> AssetType;
    /// <summary>
    /// The content type of the asset.
    /// </summary>
    public NetworkString<_32> ContentType;
    /// <summary>
    /// The content url.
    /// </summary>
    public NetworkString<_512> ContentUrl;
  }
}
#endif