﻿#if PLAYBITE_NET && PLAYBITE_NET_FUSION
using Fusion;
#endif
using UnityEngine;
using System;
using Playbite.Setup;
using Playbite.Services;
using UnityEngine.SceneManagement;
using Playbite.Models;

namespace Playbite.Components.Networking
{
  /// <summary>
  /// Component that controls the current room.
  /// </summary>
  public class PlaybiteGameRoomController : PlaybiteRoomController
  {
    [SerializeField]
    private bool AllowJoinsAfterGameStart;

    private GameSettings _gameSettings;

    /// <summary>
    /// Event triggered when another player entered the room.
    /// </summary>
    public event Action PlayerEntered;

    /// <summary>
    /// Event triggered when another player exited the room.
    /// </summary>
    public event Action PlayerExited;

    /// <summary>
    /// Event triggered when the game has started.
    /// </summary>
    public event Action GameStarted;

    /// <summary>
    /// Event triggered when the wait time has expired.
    /// </summary>
    public event Action WaitExpired;

    /// <summary>
    /// Whether the game has started.
    /// </summary>
    public bool IsGameStarted { get; set; }

#if PLAYBITE_NET && PLAYBITE_NET_FUSION
    /// <summary>
    /// Whether we're counting down to start the game.
    /// </summary>
    public bool IsCountingDown => Runner ? CountdownTimer.IsRunning && CountdownRemaining.HasValue : false;

    /// <summary>
    /// The number of seconds remaining on the countdown.
    /// </summary>
    public float? CountdownRemaining => Runner ? CountdownTimer.RemainingTime(Runner) : null;

    /// <summary>
    /// Whether we're waiting for players to join (before counting down to start).
    /// </summary>
    public bool IsWaitingForPlayers => !IsGameStarted && !IsCountingDown;

    /// <summary>
    /// The countdown timer.
    /// </summary>
    [Networked]
    public TickTimer CountdownTimer { get; set; }

    /// <summary>
    /// The wait timer.
    /// </summary>
    [Networked]
    public TickTimer WaitTimer { get; set; }

    private bool IsAuthority => Object ? HasStateAuthority : false;

    private float? WaitSeconds => _gameSettings != null && _gameSettings.MaxWaitSeconds > 0
          ? _gameSettings.MaxWaitSeconds
          : PlaybiteSettings.Default.MultiplayerSettings?.MaxWaitSeconds;

    private int MinPlayers => _gameSettings != null && _gameSettings.MinPlayers > 0
        ? _gameSettings.MinPlayers
        : PlaybiteSettings.Default.MultiplayerSettings?.MinPlayers ?? 2;

    private bool IsWaiting => Runner ? Runner.SessionInfo.PlayerCount < MinPlayers : false;

    protected void Awake()
    {
      _gameSettings = new GameService().GetGameSettings();
    }

    /// <inheritdoc />
    public override void Spawned()
    {
      base.Spawned();

      UpdateCountdown();
    }

    /// <inheritdoc />
    public override void FixedUpdateNetwork()
    {
      if (IsGameStarted) return;

      if (WaitTimer.Expired(Runner) && IsWaiting)
      {
        Runner.SessionInfo.IsOpen = false;
        WaitTimer = TickTimer.None;
        CountdownTimer = TickTimer.None;
        PlaybiteHooks.OnMatchmakingExpired();

        try
        {
          WaitExpired?.Invoke();
        }
        catch (Exception error)
        {
          PlaybiteHooks.OnMatchmakingFailed(error.Message);
          SceneManager.LoadScene(0);
          return;
        }
      }
      else if (CountdownTimer.Expired(Runner))
      {
        IsGameStarted = true;

        if (IsAuthority)
        {
          if (!AllowJoinsAfterGameStart)
            Runner.SessionInfo.IsOpen = false;
        }

        GameStarted?.Invoke();

        PlaybiteHooks.OnMatchStarted();
      }
    }

    /// <inheritdoc />
    public override void PlayerJoined(PlayerRef player)
    {
      base.PlayerJoined(player);

      Debug.Log($"Player Joined: {player.PlayerId}");

      if (IsAuthority)
      {
        UpdateCountdown();
      }

      PlayerEntered?.Invoke();
    }

    /// <inheritdoc />
    public override void PlayerLeft(PlayerRef player)
    {
      Debug.LogFormat($"Player Left: {player.PlayerId}");

      if (IsAuthority)
      {
        UpdateCountdown();
      }

      PlayerExited?.Invoke();

      base.PlayerLeft(player);
    }

    private void UpdateCountdown()
    {
      if (!IsAuthority || IsGameStarted) return;

      if (IsWaiting)
      {
        if (!WaitTimer.IsRunning
          && WaitSeconds != null
          && _gameSettings?.GameMode != "Friends"
          && _gameSettings?.IsManaged == true)
        {
          WaitTimer = TickTimer.CreateFromSeconds(Runner, WaitSeconds.Value);
        }

        CountdownTimer = TickTimer.None;
      }
      else
      {
        var remainingSeconds = CountdownRemaining ?? 0;

        var waitSecondsValue = WaitSeconds ?? 5;

        CountdownTimer = TickTimer.CreateFromSeconds(Runner, Math.Min(waitSecondsValue,
          waitSecondsValue - remainingSeconds + waitSecondsValue));

        WaitTimer = TickTimer.None;
      }
    }
#endif
  }
}
