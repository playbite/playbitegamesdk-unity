﻿#if PLAYBITE_NET && PLAYBITE_NET_FUSION
using Fusion;
using Playbite.Services;
#endif
using Playbite.Models;
using System;
using UnityEngine;

namespace Playbite.Components.Networking
{
  /// <summary>
  /// Component that controls the current room.
  /// </summary>
  public class PlaybiteRoomPlayer
#if PLAYBITE_NET && PLAYBITE_NET_FUSION
    : NetworkBehaviour
#else
    : UnityEngine.MonoBehaviour
#endif
  {
    /// <summary>
    /// Triggered when the user information is updated for this player.
    /// </summary>
    public event Action<User> UserUpdated;

#if PLAYBITE_NET && PLAYBITE_NET_FUSION
    /// <summary>
    /// The network object.
    /// </summary>
    public NetworkObject NetworkObject { get; set; }

    /// <summary>
    /// The avatar asset for this player.
    /// </summary>
    [Networked(OnChanged = nameof(OnAvatarChanged))]
    public NetworkAsset AvatarAsset { get; set; }

    /// <summary>
    /// The user info for this player.
    /// </summary>
    [Networked(OnChanged = nameof(OnUserChanged))]
    public NetworkUser User { get; set; }

    private PlaybiteAvatar _avatar;
    /// <summary>
    /// The playbite avatar for this player.
    /// </summary>
    public PlaybiteAvatar Avatar
    {
      get => _avatar;
      set
      {
        _avatar = value;
        RefreshAvatar();
      }
    }

    /// <summary>
    /// Returns whether this is a local player.
    /// </summary>
    public bool IsLocalPlayer => NetworkObject == null || NetworkObject.HasStateAuthority;

    /// <summary>
    /// Whether this player should be unmanaged. This allows you to control all aspects of the
    /// player like avatar and user info.
    /// </summary>
    public bool IsUnmanaged { get; set; }

    private void Awake()
    {
      NetworkObject = GetComponent<NetworkObject>();
      _avatar = GetComponent<PlaybiteAvatar>();
    }

    /// <summary>
    /// Updates the avatar asset.
    /// </summary>
    /// <param name="avatarAsset"></param>
    public void UpdateAvatar(Asset avatarAsset)
    {
      if (!HasStateAuthority) return;

      SendAvatarUpdate(avatarAsset);
    }

    /// <summary>
    /// Updates the user info for this player.
    /// </summary>
    /// <param name="user"></param>
    public void UpdateUser(User user)
    {
      if (!HasStateAuthority) return;

      SendUserUpdate(user);
    }

    public override void Spawned()
    {
      if (IsLocalPlayer && !IsUnmanaged)
      {
        Debug.Log("[Room Player] Setting up local player...");

        PlaybiteController.Instance.State.Auth.PropertyChanged += Auth_PropertyChanged;

        SendAvatarUpdate();

        PlaybiteController.Instance.Actions.Auth.Refresh();
      }
      else
      {
        Debug.Log("[Room Player] Setting up remote player...");

        if (Avatar && Avatar.UsePlayerAvatar)
          Avatar.UsePlayerAvatar = false;
      }
    }

    public override void Despawned(NetworkRunner runner, bool hasState)
    {
      base.Despawned(runner, hasState);

      PlaybiteController.Instance.State.Auth.PropertyChanged -= Auth_PropertyChanged;
    }

    private void Auth_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      if (PlaybiteController.Instance.State.Auth.IsLoading) return;

      SendUserUpdate();
    }

    private void RefreshAvatar()
    {
      if (!Avatar) return;

      if (string.IsNullOrEmpty(AvatarAsset.ContentUrl.ToString()))
      {
        Avatar.AvatarAsset = null;
      }
      else
      {
        Avatar.AvatarAsset = new Asset
        {
          AssetType = AvatarAsset.AssetType.ToString(),
          ContentType = AvatarAsset.ContentType.ToString(),
          ContentUrl = AvatarAsset.ContentUrl.ToString(),
        };
      }
    }

    public static void OnAvatarChanged(Changed<PlaybiteRoomPlayer> changed)
    {
      try
      {
        if (changed.Behaviour.Avatar)
        {
          changed.Behaviour.RefreshAvatar();
        }
      }
      catch (Exception err)
      {
        Debug.LogError(err);
      }
    }

    public static void OnUserChanged(Changed<PlaybiteRoomPlayer> changed)
    {
      try
      {
        changed.Behaviour.UserUpdated?.Invoke(new User
        {
          Id = changed.Behaviour.User.Id.ToString(),
          DisplayName = changed.Behaviour.User.DisplayName.ToString(),
          ImageUrl = changed.Behaviour.User.ImageUrl.ToString(),
        });
      }
      catch (Exception err)
      {
        Debug.LogError(err);
      }
    }

    private void SendAvatarUpdate()
    {
      try
      {
        var userService = new UserService();
        var asset = userService.GetAvatar();

        SendAvatarUpdate(asset);
      }
      catch (Exception err)
      {
        Debug.LogError(err);
      }
    }

    private void SendAvatarUpdate(Asset asset)
    {
      Debug.Log($"Sending avatar update... Content Url = {asset?.ContentUrl}");

      try
      {
        AvatarAsset = new NetworkAsset
        {
          AssetType = asset?.AssetType,
          ContentType = asset?.ContentType,
          ContentUrl = asset?.ContentUrl,
        };
      }
      catch (Exception err)
      {
        Debug.LogError(err);
      }
    }

    private void SendUserUpdate()
    {
      try
      {
        SendUserUpdate(PlaybiteController.Instance.State?.Auth?.User);
      }
      catch (Exception err)
      {
        Debug.LogError(err);
      }
    }

    private void SendUserUpdate(User user)
    {
      Debug.Log($"Sending user update... Display Name = {user?.DisplayName}");

      try
      {
        User = new NetworkUser
        {
          Id = user?.Id,
          DisplayName = user?.DisplayName,
          ImageUrl = user?.ImageUrl,
        };
      }
      catch (Exception err)
      {
        Debug.LogError(err);
      }
    }
#endif
  }
}
