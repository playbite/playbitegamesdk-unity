﻿#if PLAYBITE_NET && PLAYBITE_NET_FUSION
using Fusion;
using UnityEngine.SceneManagement;
#endif
using UnityEngine;
using System;
using Playbite.Helpers;
using System.Collections.Generic;
using Playbite.Models;

namespace Playbite.Components.Networking
{
  /// <summary>
  /// Component that controls the current room.
  /// </summary>
  public class PlaybiteRoomController
#if PLAYBITE_NET && PLAYBITE_NET_FUSION
    : NetworkBehaviour, IPlayerLeft, IPlayerJoined
#else
    : MonoBehaviour
#endif
  {
#if PLAYBITE_NET && PLAYBITE_NET_FUSION
    [SerializeField]
    private NetworkObject PlayerObjectPrefab;

    private Dictionary<PlayerRef, string> _playerUserIds = new Dictionary<PlayerRef, string>();

    /// <summary>
    /// Retrieves user id for a player in the room.
    /// </summary>
    /// <param name="player"></param>
    /// <returns></returns>
    public string GetPlayerUserId(PlayerRef player)
    {
      var runnerUserId = Runner.GetPlayerUserId(player);
      if (string.IsNullOrEmpty(runnerUserId))
      {
        if (_playerUserIds.TryGetValue(player, out var storedUserId))
        {
          return storedUserId;
        }
      }
      return runnerUserId;
    }

    private string NoticePlayerUserId(PlayerRef player)
    {
      var userId = GetPlayerUserId(player);
      if (!string.IsNullOrEmpty(userId))
      {
        _playerUserIds[player] = userId;
      }
      return userId;
    }

    /// <inheritdoc />
    public override void Spawned()
    {
      base.Spawned();

      if (PlayerObjectPrefab)
      {
        var playerObject = Runner.Spawn(PlayerObjectPrefab);
        Runner.SetPlayerObject(Runner.LocalPlayer, playerObject);
      }
    }

    public virtual void PlayerJoined(PlayerRef player)
    {
      if (Runner.LocalPlayer != player)
      {
        PlaybiteHooks.OnPlayerJoined(NoticePlayerUserId(player));
      }
      else
      {
        foreach (var p in Runner.ActivePlayers)
        {
          if (p != player)
          {
            PlaybiteHooks.OnPlayerJoined(NoticePlayerUserId(p));
          }
        }
      }
    }

    public virtual void PlayerLeft(PlayerRef player)
    {
      if (Runner.LocalPlayer == player)
      {
        Debug.Log("Player has left room.");

        SceneManager.LoadScene(0);
      }
      else
      {
        PlaybiteHooks.OnPlayerLeft(GetPlayerUserId(player));
        _playerUserIds.Remove(player);
      }
    }

    /// <summary>
    /// Makes a reaction in this room.
    /// </summary>
    /// <param name="from"></param>
    /// <param name="reaction"></param>
    [Rpc(RpcSources.All, RpcTargets.All)]
    public void ReactionRpc(string from, string emoji)
    {
      PlaybiteController.Instance.Tracking.TrackEvent("game_reaction", new ReactionEventParams
      {
        from = from,
        emoji = emoji
      });
    }

    [Serializable]
    public class ReactionEventParams
    {
      public string from;
      public string emoji;
    }

    /// <summary>
    /// Sends a score update.
    /// </summary>
    /// <param name="user"></param>
    /// <param name="score"></param>
    [Rpc(RpcSources.All, RpcTargets.All)]
    public void UpdateScoreRpc(string user, int score)
    {
      PlaybiteHooks.OnScoreUpdate(score, user);
    }

    /// <summary>
    /// Signals that the given user completed their game.
    /// </summary>
    /// <param name="user"></param>
    /// <param name="score"></param>
    /// <param name="achievements"></param>
    [Rpc(RpcSources.All, RpcTargets.All, InvokeLocal = false)]
    public void CompleteGameRpc(string user, int score, string achievements)
    {
      PlaybiteHooks.OnGameComplete(score, SerializationHelper.FromJson<GameAchievement[]>(achievements), user);
    }
#endif
  }
}
