﻿using System.Linq;
using Playbite.Extensions;
using Playbite.Models;
using Playbite.State;
using UnityEngine;

namespace Playbite.Components
{
	/// <summary>
	/// A playbite asset script that will auto-refresh with the player's current assets.
	/// </summary>
	public class PlaybiteAsset : MonoBehaviour
	{
		[SerializeField]
		private string _assetType = "avatar";

		[SerializeField]
		private Transform _anchor;

		[SerializeField]
		private GameObject _baseAssetPrefab;

		[SerializeField]
		private bool _usePlayerAsset = true;
		/// <summary>
    /// Gets or sets whether to use the current player's asset.
    /// </summary>
		public bool UsePlayerAsset
		{
			get => _usePlayerAsset;
			set
      {
				_usePlayerAsset = value;
				RefreshAsset();
      }
		}

		[SerializeField]
		private Asset _asset;
		/// <summary>
    /// Gets or sets the asset to load.
    /// </summary>
		public Asset Asset
		{
			get => _asset;
			set
			{
				_asset = value;
				RefreshAsset();
			}
		}

		private PlayerState PlayerState => PlaybiteController.Instance.State?.Player;
		private Asset _loadedAsset;
		private GameObject _loadedPrefab;

		/// <summary>
		/// Called when the asset has been refreshed.
		/// </summary>
		public event System.Action<GameObject> AssetRefreshed;

		protected void Start()
		{
			if (PlayerState != null)
				PlayerState.PropertyChanged += PlayerState_PropertyChanged;

			PlaybiteController.Instance.Actions.Player.RefreshAssets();

			RefreshAsset();
		}

		protected void OnDestroy()
		{
			if (PlayerState != null)
				PlayerState.PropertyChanged -= PlayerState_PropertyChanged;
		}

		private void PlayerState_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			RefreshAsset();
		}

		private void RefreshAsset()
		{
			if (UsePlayerAsset && PlayerState.IsLoading) return;

			Debug.Log("[Playbite] Refreshing asset...");

			if (UsePlayerAsset)
			{
				var loadedAsset = PlayerState.LoadedAssets?.FirstOrDefault(x => x.Asset.AssetType == _assetType);

				RefreshAsset(loadedAsset?.Asset, loadedAsset?.Prefab);
			}
			else if (Asset != null)
			{
				if (_loadedAsset == Asset) return;

				PlaybiteController.Instance.Actions.Asset.DownloadAndLoadAsset(Asset, loadedPrefab =>
				{
					RefreshAsset(Asset, loadedPrefab);
				});
			}
			else
			{
				RefreshAsset(null, null);
			}
		}

		private void RefreshAsset(Asset asset, GameObject prefab)
    {
			if (prefab == null)
      {
				Debug.Log($"[Playbite] No custom asset used, using the base asset for '{_assetType}'...");
				prefab = _baseAssetPrefab;
			}

			if (prefab == null)
      {
				Debug.Log("[Playbite] No prefab found, aborting...");
				return;
      }

			if (_loadedPrefab == prefab)
			{
				Debug.Log("[Playbite] New asset is the same as the old one, doing nothing...");
				return;
			}

			_loadedPrefab = prefab;
			_asset = _loadedAsset = asset;

			var parent = _anchor != null ? _anchor : transform;
			parent.DestroyChildren();

			var assetInstance = Instantiate(prefab, parent);

			if (assetInstance)
			{
				Debug.Log($"[Playbite] Asset set up complete for type '{_assetType}'.");

				assetInstance.transform.localPosition = Vector3.zero;
				assetInstance.transform.localRotation = Quaternion.identity;
				assetInstance.SetActive(true);
			}
			else
      {
				Debug.Log($"[Playbite] Could not set up asset for type '{_assetType}'.");
      }

			AssetRefreshed?.Invoke(assetInstance);
		}
	}
}
