﻿using Playbite.Extensions;
using Playbite.Models;
using Playbite.State;
using UnityEngine;

namespace Playbite.Components
{
	/// <summary>
	/// A playbite character script that will auto-refresh with the player's current character.
	/// </summary>
	public class PlaybiteAvatar : MonoBehaviour
	{
		[SerializeField]
		private Transform _anchor;

		[SerializeField]
		private GameObject _baseAvatarPrefab;

		[SerializeField]
		private bool _usePlayerAvatar = true;
		/// <summary>
    /// Gets or sets whether to use the current player's avatar.
    /// </summary>
		public bool UsePlayerAvatar
		{
			get => _usePlayerAvatar;
			set
      {
				_usePlayerAvatar = value;
				RefreshAvatar();
      }
		}

		[SerializeField]
		private Asset _avatarAsset;
		/// <summary>
    /// Gets or sets the avatar asset to load.
    /// </summary>
		public Asset AvatarAsset
		{
			get => _avatarAsset;
			set
			{
				_avatarAsset = value;
				RefreshAvatar();
			}
		}

		private PlayerState PlayerState => PlaybiteController.Instance.State?.Player;
		private Asset _loadedAsset;
		private GameObject _loadedPrefab;

		public event System.Action<GameObject> AvatarRefreshed;

		protected void Start()
		{
			if (PlayerState != null)
				PlayerState.PropertyChanged += PlayerState_PropertyChanged;

			PlaybiteController.Instance.Actions.Player.RefreshAssets();

			RefreshAvatar();
		}

		protected void OnDestroy()
		{
			if (PlayerState != null)
				PlayerState.PropertyChanged -= PlayerState_PropertyChanged;
		}

		private void PlayerState_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			RefreshAvatar();
		}

		private void RefreshAvatar()
		{
			if (UsePlayerAvatar && PlayerState.IsLoading) return;

			Debug.Log("[Playbite] Refreshing avatar...");

			if (UsePlayerAvatar)
			{
				RefreshAvatar(PlayerState.AvatarAsset, PlayerState.AvatarPrefab);
			}
			else if (AvatarAsset != null)
			{
				if (_loadedAsset == AvatarAsset) return;

				PlaybiteController.Instance.Actions.Asset.DownloadAndLoadAsset(AvatarAsset, loadedPrefab =>
				{
					RefreshAvatar(AvatarAsset, loadedPrefab);
				});
			}
			else
			{
				RefreshAvatar(null, null);
			}
		}

		private void RefreshAvatar(Asset avatarAsset, GameObject avatarPrefab)
    {
			if (avatarPrefab == null)
      {
				Debug.Log("[Playbite] No custom avatar used, using the base avatar...");
				avatarPrefab = _baseAvatarPrefab;
			}

			if (avatarPrefab == null)
      {
				Debug.Log("[Playbite] No avatar prefab found, aborting...");
				return;
      }

			if (_loadedPrefab == avatarPrefab)
			{
				Debug.Log("[Playbite] New avatar is the same as the old one, doing nothing...");
				return;
			}

			_loadedPrefab = avatarPrefab;
			_avatarAsset = _loadedAsset = avatarAsset;

			var parent = _anchor != null ? _anchor : transform;
			parent.DestroyChildren();

			var avatar = Instantiate(avatarPrefab, parent);

			if (avatar)
			{
				Debug.Log("[Playbite] Avatar set up complete.");

				avatar.transform.localPosition = Vector3.zero;
				avatar.transform.localRotation = Quaternion.identity;
				avatar.SetActive(true);
			}
			else
      {
				Debug.Log("[Playbite] Could not set up avatar.");
      }

			AvatarRefreshed?.Invoke(avatar);
		}
	}
}
