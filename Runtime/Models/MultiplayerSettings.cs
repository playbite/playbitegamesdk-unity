﻿using System;
using Newtonsoft.Json;
using Playbite.Components.Networking;
using UnityEngine;
using UnityEngine.Scripting;

namespace Playbite.Models
{
  /// <summary>
  /// The multiplayer settings model.
  /// </summary>
  [Serializable]
  public class MultiplayerSettings
  {
    [Preserve]
    public MultiplayerSettings()
    {
    }

    [SerializeField]
    private int maxPlayers = 2;
    /// <summary>
    /// The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created.
    /// </summary>
    [JsonProperty("maxPlayers")]
    public int MaxPlayers { get => maxPlayers; set => maxPlayers = value; }

    [SerializeField]
    private int minPlayers = 2;
    /// <summary>
    /// The minimum number of players required to start a game.
    /// </summary>
    [JsonProperty("minPlayers")]
    public int MinPlayers { get => minPlayers; set => minPlayers = value; }

    [SerializeField]
    private float maxWaitSeconds = 3;
    /// <summary>
    /// The maximum seconds to wait for other players until giving up.
    /// </summary>
    [JsonProperty("maxWaitSeconds")]
    public float MaxWaitSeconds { get => maxWaitSeconds; set => maxWaitSeconds = value; }

    [SerializeField]
    private string gameScene = null;
    /// <summary>
    /// The game scene to load.
    /// </summary>
    [JsonProperty("gameScene")]
    public string GameScene { get => gameScene; set => gameScene = value; }


    [SerializeField]
    private PlaybiteRoomController roomPrefab = null;
    /// <summary>
    /// The room prefab to spawn.
    /// </summary>
    [JsonProperty("roomPrefab")]
    public PlaybiteRoomController RoomPrefab { get => roomPrefab; set => roomPrefab = value; }
  }
}
