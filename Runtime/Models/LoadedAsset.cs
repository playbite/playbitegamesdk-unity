﻿using System;
using UnityEngine;

namespace Playbite.Models
{
  /// <summary>
  /// Represents a loaded asset.
  /// </summary>
  [Serializable]
  public class LoadedAsset
  {
    /// <summary>
    /// The asset definition.
    /// </summary>
    public Asset Asset { get; set; }

    /// <summary>
    /// The loaded object.
    /// </summary>
    public GameObject Prefab;
  }
}
