﻿using System;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Scripting;

namespace Playbite.Models
{
  /// <summary>
  /// The asset model.
  /// </summary>
  [Serializable]
  public class Asset
  {
    [Preserve]
    public Asset()
    {
    }

    [SerializeField]
    private string name;
    /// <summary>
    /// The name of the asset.
    /// </summary>
    [JsonProperty("name")]
    public string Name { get => name; set => name = value; }

    [SerializeField]
    private string contentUrl;
    /// <summary>
    /// The URL of the asset.
    /// </summary>
    [JsonProperty("contentUrl")]
    public string ContentUrl { get => contentUrl; set => contentUrl = value; }

    [SerializeField]
    private string contentType;
    /// <summary>
    /// The content type of the asset.
    /// </summary>
    [JsonProperty("contentType")]
    public string ContentType { get => contentType; set => contentType = value; }

    [SerializeField]
    private string imageUrl;
    /// <summary>
    /// The URL of the asset's image.
    /// </summary>
    [JsonProperty("imageUrl")]
    public string ImageUrl { get => imageUrl; set => imageUrl = value; }

    [SerializeField]
    private string assetType;
    /// <summary>
    /// The type of the asset.
    /// </summary>
    [JsonProperty("assetType")]
    public string AssetType { get => assetType; set => assetType = value; }
  }
}
