﻿using System;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Scripting;

namespace Playbite.Models
{
  /// <summary>
  /// The game model.
  /// </summary>
  [Serializable]
  public class Game
  {
    [Preserve]
    public Game()
    {
    }

    [SerializeField]
    private string id;
    /// <summary>
    /// The ID of the game.
    /// </summary>
    [JsonProperty("id")]
    public string Id { get => id; set => id = value; }

    [SerializeField]
    private string name;
    /// <summary>
    /// The name of the game.
    /// </summary>
    [JsonProperty("name")]
    public string Name { get => name; set => name = value; }

    [SerializeField]
    private string description;
    /// <summary>
    /// The description of the game.
    /// </summary>
    [JsonProperty("description")]
    public string Description { get => description; set => description = value; }

    [SerializeField]
    private string promoImageUrl;
    /// <summary>
    /// The promo image url.
    /// </summary>
    [JsonProperty("promoImageUrl")]
    public string PromoImageUrl { get => promoImageUrl; set => promoImageUrl = value; }
  }
}
