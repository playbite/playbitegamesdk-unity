﻿using System;
using UnityEngine;
using UnityEngine.Scripting;

namespace Playbite.Models
{
	[Serializable]
	public class GameAchievement
  {
		[Preserve]
		public GameAchievement()
		{
		}

		[SerializeField]
		private string name;
		/// <summary>
    /// The name of the achievement.
    /// </summary>
		public string Name { get => name; set => name = value; }

		[SerializeField]
		private double value;
		/// <summary>
    /// The value of the achievement.
    /// </summary>
		public double Value { get => value; set => this.value = value; }
	}
}