﻿using System;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Scripting;

namespace Playbite.Models
{
  /// <summary>
  /// The user model.
  /// </summary>
  [Serializable]
  public class User
  {
    [Preserve]
    public User()
    {
    }

    [SerializeField]
    private string id;
    /// <summary>
    /// The ID of the user.
    /// </summary>
    [JsonProperty("id")]
    public string Id { get => id; set => id = value; }

    [SerializeField]
    private string name;
    /// <summary>
    /// The user name.
    /// </summary>
    [JsonProperty("name")]
    public string Name { get => name; set => name = value; }

    [SerializeField]
    private string displayName;
    /// <summary>
    /// The user's display name.
    /// </summary>
    [JsonProperty("displayName")]
    public string DisplayName { get => displayName; set => displayName = value; }

    [SerializeField]
    private string imageUrl;
    /// <summary>
    /// The user's display name.
    /// </summary>
    [JsonProperty("imageUrl")]
    public string ImageUrl { get => imageUrl; set => imageUrl = value; }

    [SerializeField]
    private string email;
    /// <summary>
    /// The user's email.
    /// </summary>
    [JsonProperty("email")]
    public string Email { get => email; set => email = value; }

    [SerializeField]
    private long points;
    /// <summary>
    /// The user's points.
    /// </summary>
    [JsonProperty("points")]
    public long Points { get => points; set => points = value; }

    [SerializeField]
    private Asset avatar;
    /// <summary>
    /// The user's avatar.
    /// </summary>
    [JsonProperty("avatar")]
    public Asset Avatar { get => avatar; set => avatar = value; }
  }
}
