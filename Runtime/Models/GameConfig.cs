﻿using System;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Scripting;

namespace Playbite.Models
{
  /// <summary>
  /// The game config model used to configure a game statically.
  /// </summary>
  [Serializable]
  public class GameConfig
  {
    [Preserve]
    public GameConfig()
    {
    }

    [SerializeField]
    private float completeWaitSeconds = 1.5f;
    /// <summary>
    /// The seconds to wait for sending the game completion event to the app.
    /// </summary>
    [JsonProperty("completeWaitSeconds")]
    public float CompleteWaitSeconds { get => completeWaitSeconds; set => completeWaitSeconds = value; }
  }
}
