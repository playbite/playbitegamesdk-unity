﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Scripting;

namespace Playbite.Models
{
  /// <summary>
  /// The game settings model.
  /// </summary>
  [Serializable]
  public class GameSettings
  {
    [Preserve]
    public GameSettings()
    {
    }

    [SerializeField]
    private string gameMode;
    /// <summary>
    /// The game mode.
    /// </summary>
    [JsonProperty("gameMode")]
    public string GameMode { get => gameMode; set => gameMode = value; }

    [SerializeField]
    private string networkAppId;
    /// <summary>
    /// The networking app id.
    /// </summary>
    [JsonProperty("networkAppId")]
    public string NetworkAppId { get => networkAppId; set => networkAppId = value; }

    [SerializeField]
    private string region;
    /// <summary>
    /// The region to use for the session.
    /// </summary>
    [JsonProperty("region")]
    public string Region { get => region; set => region = value; }

    [SerializeField]
    private string sessionId;
    /// <summary>
    /// The session id.
    /// </summary>
    [JsonProperty("sessionId")]
    public string SessionId { get => sessionId; set => sessionId = value; }

    [SerializeField]
    private bool isManaged;
    /// <summary>
    /// Whether the game is managed externally.
    /// </summary>
    [JsonProperty("isManaged")]
    public bool IsManaged { get => isManaged; set => isManaged = value; }

    [SerializeField]
    private List<User> bots;
    /// <summary>
    /// The bots collection.
    /// </summary>
    [JsonProperty("bots")]
    public List<User> Bots { get => bots; set => bots = value; }

    [SerializeField]
    private int maxPlayers;
    /// <summary>
    /// The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created.
    /// </summary>
    [JsonProperty("maxPlayers")]
    public int MaxPlayers { get => maxPlayers; set => maxPlayers = value; }

    [SerializeField]
    private int minPlayers;
    /// <summary>
    /// The minimum number of players required to start a game.
    /// </summary>
    [JsonProperty("minPlayers")]
    public int MinPlayers { get => minPlayers; set => minPlayers = value; }

    [SerializeField]
    private float maxWaitSeconds;
    /// <summary>
    /// The maximum seconds to wait for other players until giving up.
    /// </summary>
    [JsonProperty("maxWaitSeconds")]
    public float MaxWaitSeconds { get => maxWaitSeconds; set => maxWaitSeconds = value; }
  }
}
