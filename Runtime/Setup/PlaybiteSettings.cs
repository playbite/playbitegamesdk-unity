﻿using System.IO;
using UnityEngine;
using Playbite.Models;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Playbite.Setup
{
	/// <summary>
	/// Settings for setup of Playbite game projects.
	/// </summary>
	public class PlaybiteSettings : ScriptableObject
	{
		private static PlaybiteSettings _defaultSettings;

		/// <summary>
		/// Gets the default settings.
		/// </summary>
		public static PlaybiteSettings Default => _defaultSettings ?? (_defaultSettings = Load());

		/// <summary>
		/// Specifies the Playbite game id.
		/// </summary>
		public string GameId;

		/// <summary>
		/// Whether the default avatar is enabled when playing in the editor.
		/// </summary>
		public bool EnableDefaultAvatarInEditor = true;

		/// <summary>
		/// Whether the default avatar is enabled when playing in the player.
		/// </summary>
		public bool EnableDefaultAvatarInPlayer = false;

		/// <summary>
    /// Defines the default avatar.
    /// </summary>
		public Asset DefaultAvatar = new Asset
		{
			ContentUrl = "https://gateway.pinata.cloud/ipfs/QmQFvJ4oTFM4mxPqPTMrXUxEYxgjGaToBCFxzouQxQP3ZH",
			ContentType = "model/gltf-binary",
			AssetType = "avatar"
		};

		/// <summary>
    /// Defines default extra assets to use.
    /// </summary>
		public Asset[] DefaultExtraAssets;

		/// <summary>
		/// The game configuration.
		/// </summary>
		public GameConfig GameConfig = null;

		/// <summary>
		/// Whether the default game settings is enabled.
		/// </summary>
		public bool EnableDefaultGameSettings = false;

		/// <summary>
		/// The default game settings to use while in the editor.
		/// </summary>
		public GameSettings DefaultGameSettings = null;

		/// <summary>
		/// Whether the default game data is enabled.
		/// </summary>
		public bool EnableDefaultGameData = false;

		/// <summary>
    /// The default game data.
    /// </summary>
		public string DefaultGameData = null;

		/// <summary>
		/// The multiplayer settings.
		/// </summary>
		public MultiplayerSettings MultiplayerSettings = null;

		/// <summary>
		/// Load the settings object.
		/// </summary>
		/// <returns></returns>
		public static PlaybiteSettings Load()
		{
			var settings = Resources.Load<PlaybiteSettings>("Playbite/Settings");

#if UNITY_EDITOR
			if (settings == null)
			{
				const string filePath = "Assets/Playbite/Resources/Playbite/Settings.asset";
				var dir = Path.GetDirectoryName(filePath);

				if (!Directory.Exists(dir))
					Directory.CreateDirectory(dir);

				if (File.Exists(filePath))
				{
					AssetDatabase.DeleteAsset(filePath);
					AssetDatabase.Refresh();
				}

				settings = CreateInstance<PlaybiteSettings>();
				AssetDatabase.CreateAsset(settings, filePath);
				AssetDatabase.Refresh();

				AssetDatabase.SaveAssets();
			}
#endif

			return settings;
		}
	}
}