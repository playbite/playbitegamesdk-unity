﻿using UnityEngine;
using UnityEngine.Scripting;

namespace Playbite.Messages
{
  /// <summary>
  /// Message for a reaction.
  /// </summary>
  public class PlaybiteReactionMessage : PlaybiteMessage
  {
    [Preserve]
    public PlaybiteReactionMessage()
    {
    }

    [SerializeField, Preserve]
    private string emoji;
    /// <summary>
    /// Specifies the emoji to send.
    /// </summary>
    public string Emoji { get => emoji; set => emoji = value; }

    [SerializeField, Preserve]
    private string from;
    /// <summary>
    /// Specifies the emoji to send.
    /// </summary>
    public string From { get => from; set => from = value; }
  }
}
