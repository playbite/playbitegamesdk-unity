﻿using UnityEngine;
using UnityEngine.Scripting;

namespace Playbite.Messages
{
  /// <summary>
  /// Message for a reaction.
  /// </summary>
  public class PlaybiteDataChangedMessage : PlaybiteMessage
  {
    [Preserve]
    public PlaybiteDataChangedMessage()
    {
    }

    [SerializeField, Preserve]
    private string dataType;
    /// <summary>
    /// Specifies the data type that changed.
    /// </summary>
    public string DataType { get => dataType; set => dataType = value; }
  }
}
