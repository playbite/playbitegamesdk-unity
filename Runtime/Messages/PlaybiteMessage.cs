﻿using UnityEngine;
using System;
using UnityEngine.Scripting;

namespace Playbite
{
  [Serializable]
  public class PlaybiteMessage
  {
    [Preserve]
    public PlaybiteMessage()
    {
    }

    [SerializeField, Preserve]
    private string action;
    /// <summary>
    /// Specifies an action to perform.
    /// </summary>
    public string Action { get => action; set => action = value; }
  }
}
