﻿using Playbite.Models;
using Playbite.Setup;
using Playbite.Helpers;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Playbite.Services
{
	/// <summary>
	/// Service for game routines.
	/// </summary>
	public class GameService
	{
		/// <summary>
		/// Retrieves the custom game data as a JSON string.
		/// </summary>
		/// <returns></returns>
		public string GetGameData()
		{
#if UNITY_WEBGL
#if UNITY_EDITOR
			return GetDefaultGameData();
#else
			return getPlaybiteGameData();
#endif
#else
			return null;
#endif
		}

		private static string GetDefaultGameData()
		{
			if (!PlaybiteSettings.Default.EnableDefaultGameData)
				return null;

			return PlaybiteSettings.Default.DefaultGameData;
		}

		/// <summary>
		/// Retrieves the game settings.
		/// </summary>
		/// <returns></returns>
		public GameSettings GetGameSettings()
		{
#if UNITY_WEBGL
#if UNITY_EDITOR
			return GetDefaultGameSettings();
#else
			var value = getPlaybiteSettings();

			return string.IsNullOrEmpty(value)
				? null
				: SerializationHelper.FromJson<GameSettings>(value);
#endif
#else
			return null;
#endif
		}

		private static GameSettings GetDefaultGameSettings()
		{
			if (!PlaybiteSettings.Default.EnableDefaultGameSettings)
				return null;

			return PlaybiteSettings.Default.DefaultGameSettings;
		}

		/// <summary>
		/// Retrieves the custom game data as a JSON string.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<User> GetBots()
		{
#if UNITY_WEBGL
			return GetGameSettings()?.Bots ?? Enumerable.Empty<User>();
#else
			return new List<User>();
#endif
		}

		/// <summary>
    /// Gets a random bot.
    /// </summary>
    /// <returns></returns>
		public User GetRandomBot()
		{
			return GetBots().OrderBy(x => Random.value).FirstOrDefault();
		}

#if UNITY_WEBGL
		[DllImport("__Internal")]
		private static extern string getPlaybiteGameData();

		[DllImport("__Internal")]
		private static extern string getPlaybiteSettings();
#endif
	}
}