﻿using UnityEngine;
using Playbite.Setup;
using System.Runtime.InteropServices;
using Playbite.Helpers;

namespace Playbite.Services
{
	/// <summary>
	/// Service for tracking actions.
	/// </summary>
	public class TrackingService
	{
		private static bool _isInitialized;

		internal void Initialize()
		{
			if (_isInitialized)
				return;

			_isInitialized = true;

#if UNITY_EDITOR
			Debug.LogFormat("[Playbite/Tracking] Initializing tracking...");
#endif
		}

		/// <summary>
		/// Tracks the event specified by name.
		/// </summary>
		/// <param name="name"></param>
		public void TrackEvent(string name) => TrackEvent(name, null);

		/// <summary>
		/// Tracks the event specified by name with one parameter.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="payload"></param>
		public void TrackEvent(string name, object payload)
		{
			try
			{
				var payloadStr = payload != null ? SerializationHelper.ToJson(payload) : null;

				Debug.LogFormat("[Playbite] Event = {0} with parameters {1}", name, payloadStr);

#if UNITY_WEBGL && !UNITY_EDITOR
				trackPlaybiteEvent(name, payloadStr);
#endif
			}
			catch (System.Exception error)
			{
				Debug.Log($"[Playbite] Encountered error while tracking event: {error.Message}");
			}
		}

		/// <summary>
		/// Tracks when a new matchmaking session has started.
		/// </summary>
		public void TrackMatchmakingStarted()
		{
			TrackEvent("matchmaking_start");
		}

		/// <summary>
		/// Tracks when a new game has started.
		/// </summary>
		public void TrackGameStarted()
		{
			TrackEvent("game_start");
		}

		/// <summary>
		/// Tracks when a game has ended.
		/// </summary>
		public void TrackGameEnded()
		{
			TrackEvent("game_end");
		}

#if UNITY_WEBGL
		[DllImport("__Internal")]
		public static extern string trackPlaybiteEvent(string eventName, string payload);
#endif
	}
}