﻿using System.Runtime.InteropServices;
using Playbite.Models;
using Playbite.Setup;
using Playbite.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Playbite.Services
{
	/// <summary>
	/// Service for users.
	/// </summary>
	public class UserService
	{
		/// <summary>
		/// Retrieves information about the current user.
		/// </summary>
		/// <returns></returns>
		public User GetMe()
		{
#if UNITY_WEBGL
#if UNITY_EDITOR
			return new User
			{
				Id = "123456789",
				DisplayName = "Costanza",
				ImageUrl = "https://image.playbite.com/platform/images/profile/auto/blob3.png"
			};
#else
			var value = getPlaybiteUser();

			return string.IsNullOrEmpty(value)
				? null
				: SerializationHelper.FromJson<User>(value);
#endif
#else
			return null;
#endif
		}

		/// <summary>
		/// Gets the avatar asset selected by the current user.
		/// </summary>
		/// <returns></returns>
		public Asset GetAvatar()
		{
#if UNITY_WEBGL
#if UNITY_EDITOR || !PLAYBITE_AVATARS
			return GetDefaultAvatar();
#else
			var value = getPlaybiteAvatar();

			return string.IsNullOrEmpty(value)
				? GetDefaultAvatar()
				: SerializationHelper.FromJson<Asset>(value);
#endif
#else
			return GetDefaultAvatar();
#endif
		}

		/// <summary>
    /// Gets a sequence of assets for this user.
    /// </summary>
    /// <returns></returns>
		public IEnumerable<Asset> GetAssets()
    {
      var assets = new List<Asset>
      {
        GetAvatar()
      };

			assets.AddRange(GetExtraAssets());

      return assets
				.Where(x => x != null && !string.IsNullOrEmpty(x.ContentUrl));
    }

		private static Asset GetDefaultAvatar()
		{
#if PLAYBITE_AVATARS
			if (!string.IsNullOrEmpty(PlaybiteSettings.Default.DefaultAvatar?.ContentUrl))
			{
				if (!PlaybiteSettings.Default.EnableDefaultAvatarInEditor)
					return null;

#if !UNITY_EDITOR
				if (!PlaybiteSettings.Default.EnableDefaultAvatarInPlayer)
					return null;
#endif

				return new Asset
				{
					ContentUrl = PlaybiteSettings.Default.DefaultAvatar.ContentUrl,
					ContentType = PlaybiteSettings.Default.DefaultAvatar.ContentType,
					AssetType = "avatar",
				};
			}
#endif
			return null;
		}

		private static IEnumerable<Asset> GetExtraAssets()
    {
#if PLAYBITE_AVATARS
			if (!PlaybiteSettings.Default.EnableDefaultAvatarInEditor)
				return Enumerable.Empty<Asset>();

#if !UNITY_EDITOR
			if (!PlaybiteSettings.Default.EnableDefaultAvatarInPlayer)
				return Enumerable.Empty<Asset>();
#endif
			if (PlaybiteSettings.Default.DefaultExtraAssets == null)
				return Enumerable.Empty<Asset>();

			return PlaybiteSettings.Default.DefaultExtraAssets
				.Where(x => x != null && !string.IsNullOrEmpty(x.ContentUrl) && !string.IsNullOrEmpty(x.AssetType))
				.Select(asset => new Asset
				{
					ContentUrl = asset.ContentUrl,
					ContentType = asset.ContentType,
					AssetType = asset.AssetType
				});
#else
			return Enumerable.Empty<Asset>();
#endif
		}

#if UNITY_WEBGL
		[DllImport("__Internal")]
		private static extern string getPlaybiteUser();

		[DllImport("__Internal")]
		private static extern string getPlaybiteAvatar();
#endif
	}
}