﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Object = UnityEngine.Object;
using Playbite.Extensions;
using Playbite.Helpers;

namespace Playbite.Services
{
  /// <summary>
  /// Service for handling downloadable content.
  /// </summary>
  public class ContentService
  {
    /// <summary>
    /// Downloads and loads an asset given the specified information (co-routine).
    /// </summary>
    /// <param name="url"></param>
    /// <param name="contentType"></param>
    /// <param name="assetName"></param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    public IEnumerator DownloadAndLoadAssetCo(string url, string contentType, string assetName, Action<GameObject> onComplete)
    {
      if (contentType == "model/gltf-binary" || contentType == "model/gltf+json")
      {
#if PLAYBITE_AVATARS
        if (contentType == "model/gltf-binary" && !url.EndsWith(".glb"))
          url += $"?filename=model.glb";
        else if (contentType == "model/gltf+json" && !url.EndsWith(".gltf"))
          url += $"?filename=model.gltf";

        Debug.Log($"[Playbite] Downloading asset from {url}");

        var gltf = new GLTFast.GltfImport(new CachedDownloadProvider(), logger: new BasicLogger());
        var loadTask = gltf.Load(url);

        yield return new WaitUntil(() => loadTask.IsCompleted);

        if (!gltf.LoadingDone || gltf.LoadingError)
        {
          if (gltf.LoadingError)
            Debug.LogError("[Playbite] Had a loading error!");

          if (loadTask.Exception != null)
            Debug.LogError(loadTask.Exception);

          onComplete(null);
        }
        else
        {
          var gameObject = new GameObject(assetName);
          gameObject.hideFlags = HideFlags.DontSave;
          gameObject.SetActive(false);
          Object.DontDestroyOnLoad(gameObject);

          gltf.InstantiateMainScene(gameObject.transform);

          onComplete(gameObject);
        }
#else
				yield return new WaitForEndOfFrame();
				onComplete(null);
#endif
      }
      else if (contentType?.StartsWith("image/") ?? false)
      {
        var www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        var texture = DownloadHandlerTexture.GetContent(www).Duplicate();
        
        var backgroundColor = texture.GetPixel(50, 50);
        for (int x = 0; x < texture.width; ++x)
          for (int y = 0; y < texture.height; ++y)
          {
            if (texture.GetPixel(x, y) == backgroundColor)
            {
              texture.SetPixel(x, y, new Color(0, 0, 0, 0));
            }
          }

        texture.Apply();

        var gameObject = new GameObject(assetName);
        gameObject.hideFlags = HideFlags.DontSave;
        gameObject.SetActive(false);
        Object.DontDestroyOnLoad(gameObject);

        var faces = new[]
        {
          GameObject.CreatePrimitive(PrimitiveType.Quad),
          GameObject.CreatePrimitive(PrimitiveType.Quad)
        };

        for (var i = 0; i < faces.Length; ++i)
        {
          var face = faces[i];
          face.transform.parent = gameObject.transform;
          face.transform.localPosition = Vector3.one * 0.5f;
          if (i == 1)
          {
            face.transform.localEulerAngles = new Vector3(0, 180, 0);
            face.transform.localScale = new Vector3(-1, 1, 1);
          }

          var material = face.GetComponent<MeshRenderer>().material;
          material.ChangeRenderMode(BlendMode.Fade);

          material.mainTexture = texture;
        }
        onComplete(gameObject);
      }
      else
      {
        DownloadBundleAndLoadAsset(url, assetName, onComplete);
      }
    }

    /// <summary>
    /// Downloads and loads an asset given the specified information.
    /// </summary>
    /// <param name="url"></param>
    /// <param name="contentType"></param>
    /// <param name="assetName"></param>
    /// <param name="onComplete"></param>
    public void DownloadAndLoadAsset(string url, string contentType, string assetName, Action<GameObject> onComplete)
    {
      PlaybiteController.Instance.StartCoroutine(DownloadAndLoadAssetCo(url,
        contentType, assetName,
        assetBundle => onComplete(assetBundle)
      ));
    }

    /// <summary>
    /// Downloads and loads a texture given the specified information (co-routine).
    /// </summary>
    /// <param name="url"></param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    public IEnumerator DownloadAndLoadTextureCo(string url, Action<Texture2D> onComplete)
    {
      var www = UnityWebRequestTexture.GetTexture(url);
      yield return www.SendWebRequest();

      var texture = DownloadHandlerTexture.GetContent(www);

      onComplete(texture ? texture.Duplicate() : null);
    }

    /// <summary>
    /// Downloads and loads a texture given the specified information.
    /// </summary>
    /// <param name="url"></param>
    /// <param name="onComplete"></param>
    public void DownloadAndLoadTexture(string url, Action<Texture2D> onComplete)
    {
      PlaybiteController.Instance.StartCoroutine(DownloadAndLoadTextureCo(url,
        texture => onComplete(texture)
      ));
    }

    /// <summary>
    /// Downloads a bundle from the specified url in a co-routine.
    /// </summary>
    /// <param name="url"></param>
    /// <param name="onComplete"></param>
    public IEnumerator DownloadBundleCo(string url, Action<AssetBundle> onComplete)
    {
      var request = UnityWebRequestAssetBundle.GetAssetBundle(url, 0);

      yield return request.SendWebRequest();

      onComplete(DownloadHandlerAssetBundle.GetContent(request));
    }

    /// <summary>
    /// Downloads a bundle from the specified url.
    /// </summary>
    /// <param name="url"></param>
    /// <param name="onComplete"></param>
    public void DownloadBundle(string url, Action<AssetBundle> onComplete)
    {
      PlaybiteController.Instance.StartCoroutine(DownloadBundleCo(url,
        assetBundle => onComplete(assetBundle)
      ));
    }

    /// <summary>
    /// Downloads and unpacks the given url.
    /// </summary>
    /// <param name="url"></param>
    /// <param name="onComplete"></param>
    public void DownloadBundleAndUnpack(string url, Action<Object[]> onComplete)
    {
      PlaybiteController.Instance.StartCoroutine(DownloadBundleCo(url,
        assetBundle =>
        {
          if (assetBundle == null)
          {
            onComplete(null);
            return;
          }
          PlaybiteController.Instance.StartCoroutine(LoadAllAssetsFromBundle(assetBundle,
            assets => onComplete(assets)
          ));
        }
      ));
    }

    /// <summary>
    /// Downloads and unpacks the given url.
    /// </summary>
    /// <param name="url"></param>
    /// <param name="assetName"></param>
    /// <param name="onComplete"></param>
    public void DownloadBundleAndLoadAsset(string url, string assetName, Action<GameObject> onComplete)
    {
      PlaybiteController.Instance.StartCoroutine(DownloadBundleCo(url,
        assetBundle =>
        {
          if (assetBundle == null)
          {
            onComplete(null);
            return;
          }
          PlaybiteController.Instance.StartCoroutine(LoadAssetFromBundle(assetBundle, assetName,
            asset => onComplete(asset)
          ));
        }
      ));
    }

    private IEnumerator LoadAssetFromBundle(AssetBundle assetBundle, string assetName, Action<GameObject> onComplete)
    {
      var bundleRequest = assetBundle.LoadAssetAsync(assetName);
      yield return bundleRequest;
      onComplete(bundleRequest.asset as GameObject);
    }

    private IEnumerator LoadAllAssetsFromBundle(AssetBundle assetBundle, Action<Object[]> onComplete)
    {
      var bundleRequest = assetBundle.LoadAllAssetsAsync();
      yield return bundleRequest;
      onComplete(bundleRequest.allAssets);
    }
  }
}
