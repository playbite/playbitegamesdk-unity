﻿using UnityEngine;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using Playbite.Models;
using System.Linq;
using Playbite.Helpers;
using Playbite.Services;
using Playbite.Extensions;
using Playbite.Setup;

namespace Playbite
{
	/// <summary>
	/// The access point for controlling the Playbite SDK integration.
	/// </summary>
	public static class PlaybiteHooks
	{
		private static float CompleteWaitSeconds => PlaybiteSettings.Default.GameConfig?.CompleteWaitSeconds ?? 1.5f;

#if PLAYBITE_NET
		/// <summary>
		/// This should be called when a multiplayer matchmaking session is started.
		/// PlaybiteLauncher already does this for you.
		/// </summary>
		public static void OnMatchmakingStarted()
		{
			PlaybiteController.Instance.Tracking.TrackEvent("matchmaking_started");
		}

		/// <summary>
		/// This should be called when a multiplayer matchmaking session is complete and the user joins a room.
		/// PlaybiteLauncher already does this for you.
		/// </summary>
		public static void OnMatchmakingComplete()
		{
			PlaybiteController.Instance.Tracking.TrackEvent("matchmaking_complete");
		}

		/// <summary>
		/// This should be called when a user has waited too long for a multiplayer session to start.
		/// PlaybiteGameRoomController already does this for you.
		/// </summary>
		public static void OnMatchmakingExpired()
		{
			PlaybiteController.Instance.Tracking.TrackEvent("matchmaking_expired");
		}

		/// <summary>
		/// This should be called when a multiplayer match has started.
		/// PlaybiteGameRoomController already does this for you.
		/// </summary>
		public static void OnMatchStarted()
		{
			PlaybiteController.Instance.Tracking.TrackEvent("match_started");
		}

		/// <summary>
		///	This should be called when matchmaking failed.
		/// PlaybiteLauncher already does this for you.
		/// </summary>
		/// <param name="errorMessage"></param>
		public static void OnMatchmakingFailed(string errorMessage = "Unknown error")
		{
			PlaybiteController.Instance.Tracking.TrackEvent("matchmaking_failed",
				new ErrorParams { errorMessage = errorMessage });
		}

		[Serializable]
		public class ErrorParams
		{
			public string errorMessage;
		}

		/// <summary>
		///	This should be called when a player joined a multiplayer session.
		/// PlaybiteRoomController already does this for you.
		/// </summary>
		/// <param name="user"></param>
    /// <param name="isBot"></param>
		public static void OnPlayerJoined(string user, bool isBot = false)
		{
			PlaybiteController.Instance.Tracking.TrackEvent("player_joined",
				new MatchmakingPlayerJoinParams { user = user, isBot = isBot });
		}

		/// <summary>
		///	This should be called when a player left a multiplayer session.
		/// PlaybiteRoomController already does this for you.
		/// </summary>
		/// <param name="user"></param>
		public static void OnPlayerLeft(string user)
		{
			PlaybiteController.Instance.Tracking.TrackEvent("player_left",
				new MatchmakingPlayerLeftParams { user = user });
		}

		[Serializable]
		public class MatchmakingPlayerJoinParams
		{
			public string user;
			public bool isBot;
		}

		[Serializable]
		public class MatchmakingPlayerLeftParams
		{
			public string user;
		}
#endif

		/// <summary>
		/// This should be called when a new game is started.
		/// </summary>
		public static void OnGameStarted()
		{
			PlaybiteController.Instance.Tracking.TrackGameStarted();
		}

		/// <summary>
		/// This should be called when a game is complete.
		/// </summary>
		/// <param name="score"></param>
		public static void OnGameComplete(int score)
		{
			OnGameComplete(score, null);
		}

		/// <summary>
		/// This should be called when a game is complete.
		/// </summary>
		/// <param name="score"></param>
		/// <param name="achievements"></param>
		public static void OnGameComplete(int score, IEnumerable<GameAchievement> achievements)
		{
			PlaybiteController.Instance.Tracking.TrackGameEnded();

#if PLAYBITE_NET && PLAYBITE_NET_FUSION
			if (PlaybiteController.Instance.CurrentRoom)
			{
				PlaybiteController.Instance.CurrentRoom.CompleteGameRpc(
					PlaybiteController.Instance.State.Auth?.User?.DisplayName,
					score,
					SerializationHelper.ToJson(achievements)
				);
			}
#endif
			OnGameComplete(score, achievements, PlaybiteController.Instance.State.Auth?.User?.DisplayName, true);
		}

		/// <summary>
		/// This should be called when a game is complete.
		/// </summary>
		/// <param name="score"></param>
		/// <param name="achievements"></param>
		public static void OnGameComplete(int score, IEnumerable<GameAchievement> achievements, string user, bool isLocalPlayer = false)
		{
			if (isLocalPlayer)
      {
				PlaybiteController.Instance.PerformWithRealDelay(CompleteWaitSeconds,
					() => SendCompleteEvent(score, achievements, user));
      }
			else
      {
				SendCompleteEvent(score, achievements, user);
      }
		}

		private static void SendCompleteEvent(int score, IEnumerable<GameAchievement> achievements, string user)
    {
#if UNITY_WEBGL
			Debug.Log($"[Playbite] Completing game with {score} points for {user ?? "Anonymous"}...");

			var payload = new GameCompleteParams
			{
				user = user,
				points = score,
				achievements = achievements?.ToList()
			};

			var payloadStr = SerializationHelper.ToJson(payload);

#if UNITY_EDITOR
			Debug.LogFormat("[Playbite] Sending completion event: {0}", payloadStr);
#endif

#if !UNITY_EDITOR
			onGameComplete(payloadStr);
#endif
#endif
		}

		[Serializable]
		public class GameCompleteParams
		{
			public string user;
			public int points;
			public List<GameAchievement> achievements;
		}

		/// <summary>
		/// This should be called when there is a score update for the local user.
		/// </summary>
		/// <param name="score"></param>
		public static void OnScoreUpdate(int score)
		{
#if PLAYBITE_NET && PLAYBITE_NET_FUSION
			if (PlaybiteController.Instance.CurrentRoom)
			{
				PlaybiteController.Instance.CurrentRoom.UpdateScoreRpc(PlaybiteController.Instance.State.Auth?.User?.DisplayName, score);
				return;
			}
#endif
			OnScoreUpdate(score, PlaybiteController.Instance.State.Auth?.User?.DisplayName);
		}

		/// <summary>
		/// This should be called when there is a score update for the given user.
		/// </summary>
		/// <param name="score"></param>
		public static void OnScoreUpdate(int score, string user)
		{
			PlaybiteController.Instance.Tracking.TrackEvent("game_score", new ScoreUpdateEventParams
			{
				user = user,
				points = score
			});
		}

		[Serializable]
		public class ScoreUpdateEventParams
		{
			public string user;
			public int points;
		}

		/// <summary>
		/// Opens a user profile in the app.
		/// </summary>
		/// <param name="displayName"></param>
		public static void OpenUserProfile(string displayName)
		{
#if UNITY_WEBGL
			Debug.Log($"[Playbite] Opening profile for {displayName}...");

#if !UNITY_EDITOR
			openUserProfile(displayName);
#endif
#endif
		}

		/// <summary>
		/// Shows the reactions component.
		/// </summary>
		public static void ShowReactions()
		{
#if UNITY_WEBGL
			Debug.Log($"[Playbite] Showing reactions...");

#if !UNITY_EDITOR
			showReactions();
#endif
#endif
		}

		/// <summary>
		/// Hides the reactions component.
		/// </summary>
		public static void HideReactions()
		{
#if UNITY_WEBGL
			Debug.Log($"[Playbite] Hiding reactions...");

#if !UNITY_EDITOR
			hideReactions();
#endif
#endif
		}

#if UNITY_WEBGL
		[DllImport("__Internal")]
		private static extern string onGameComplete(string payloadStr);

		[DllImport("__Internal")]
		private static extern string openUserProfile(string displayName);

		[DllImport("__Internal")]
		private static extern string showReactions();

		[DllImport("__Internal")]
		private static extern string hideReactions();
#endif
	}
}
