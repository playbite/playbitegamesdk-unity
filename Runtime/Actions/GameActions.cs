﻿using Playbite.Services;
using Playbite.State;
using UnityEngine;

namespace Playbite.Actions
{
	/// <summary>
	/// Actions that can update the game state.
	/// </summary>
	public class GameActions : BaseActions
	{
		private readonly GameService _gameService = new GameService();
		private GameState GameState => PlaybiteController.Instance.State.Game;

		/// <summary>
		/// Refreshes the game info.
		/// </summary>
		public void Refresh()
		{
			if (GameState.IsLoading) return;

			GameState.IsLoading = true;

			try
			{
				Debug.Log("[Playbite] Loading game settings...");

				GameState.GameSettings = _gameService.GetGameSettings();

				Debug.Log($"[Playbite] Loaded game settings.");

				Debug.Log("[Playbite] Loading game data...");

				GameState.GameData = _gameService.GetGameData();

				Debug.Log($"[Playbite] Loaded game data.");
			}
			finally
      {
				GameState.IsLoading = false;
			}
		}
	}
}
