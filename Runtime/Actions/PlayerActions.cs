﻿using System;
using System.Collections.Generic;
using System.Linq;
using Playbite.Helpers;
using Playbite.Models;
using Playbite.Services;
using Playbite.State;
using UnityEngine;

namespace Playbite.Actions
{
  /// <summary>
  /// Actions that can update the player state.
  /// </summary>
  public class PlayerActions : BaseActions
  {
    private readonly UserService _userService = new UserService();
    private PlayerState PlayerState => PlaybiteController.Instance.State.Player;
    private AssetActions AssetActions => PlaybiteController.Instance.Actions.Asset;

    /// <summary>
    /// Refreshes the user's assets.
    /// </summary>
    public void RefreshAssets()
    {
      if (PlayerState.IsLoading) return;

      PlayerState.IsLoading = true;

      Debug.Log("[Playbite] Loading player assets...");

      var assets = _userService.GetAssets();
      var validAssets = assets.Where(x => !string.IsNullOrEmpty(x.ContentUrl)).ToList();
      var loadedAssets = new List<LoadedAsset>();

      if (!assets.Any())
      {
        Debug.Log("[Playbite] No valid assets found.");

        PlayerState.IsLoading = false;
        PlayerState.AvatarAsset = null;
        PlayerState.AvatarPrefab = null;
        PlayerState.LoadedAssets = loadedAssets;
        return;
      }

      var priorLoadedAssets = PlayerState.LoadedAssets;
      var processedCount = 0;
      Action onAssetProcessed = () =>
      {
        ++processedCount;
        if (processedCount >= validAssets.Count)
        {
          PlayerState.LoadedAssets = loadedAssets;
          PlayerState.IsLoading = false;
        }
      };

      foreach (var asset in assets)
      {
        Debug.Log($"[Playbite] Found asset: {SerializationHelper.ToJson(asset)}");

        var existingAsset = priorLoadedAssets?.FirstOrDefault(x => x.Asset.ContentUrl == asset.ContentUrl);

        if (existingAsset?.Prefab != null)
        {
          Debug.Log("[Playbite] Avatar was already loaded.");
          loadedAssets.Add(existingAsset);
          onAssetProcessed();
          continue;
        }

        Debug.Log($"[Playbite] Downloading asset from URL: {asset.ContentUrl}...");

        AssetActions.DownloadAndLoadAsset(asset, prefab =>
        {
          if (prefab != null)
          {
            Debug.Log($"[Playbite] Successfully loaded asset.");

            loadedAssets.Add(new LoadedAsset
            {
              Asset = asset,
              Prefab = prefab
            });

            if (asset.AssetType == "avatar")
            {
              PlayerState.AvatarAsset = asset;
              PlayerState.AvatarPrefab = prefab;
            }
          }
          else
          {
            Debug.Log($"[Playbite] Could not load asset.");

            if (asset.AssetType == "avatar")
            {
              PlayerState.AvatarAsset = null;
              PlayerState.AvatarPrefab = null;
            }
          }

          onAssetProcessed();
        });
      }
    }
  }
}
