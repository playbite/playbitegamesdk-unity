﻿using System;

namespace Playbite.Actions
{
	/// <summary>
	/// The root app actions.
	/// </summary>
	public class AppActions : BaseActions
	{
		/// <summary>
		/// The authentication actions.
		/// </summary>
		public AuthActions Auth { get; } = new AuthActions();

		/// <summary>
		/// The player actions.
		/// </summary>
		public PlayerActions Player { get; } = new PlayerActions();

		/// <summary>
		/// The asset actions.
		/// </summary>
		public AssetActions Asset { get; } = new AssetActions();

		/// <summary>
		/// The game actions.
		/// </summary>
		public GameActions Game { get; } = new GameActions();
	}
}
