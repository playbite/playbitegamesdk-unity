﻿using Playbite.Services;
using Playbite.State;
using UnityEngine;

namespace Playbite.Actions
{
	/// <summary>
	/// Actions that can update the auth state.
	/// </summary>
	public class AuthActions : BaseActions
	{
		private readonly UserService _userService = new UserService();
		private AuthState AuthState => PlaybiteController.Instance.State.Auth;

		/// <summary>
		/// Refreshes the authentication info.
		/// </summary>
		public void Refresh()
		{
			if (AuthState.IsLoading) return;

			AuthState.IsLoading = true;

			try
			{
				Debug.Log("[Playbite] Loading user...");

				AuthState.User = _userService.GetMe();

				Debug.Log($"[Playbite] Got user '{AuthState.User?.DisplayName}'.");
			}
			finally
      {
				AuthState.IsLoading = false;
			}
		}
	}
}
