﻿using Playbite.Helpers;
using Playbite.Services;
using Playbite.State;
using Playbite.Models;
using UnityEngine;
using System;

namespace Playbite.Actions
{
	/// <summary>
	/// Actions that can update the player state.
	/// </summary>
	public class AssetActions : BaseActions
	{
		private ContentService _assetService = new ContentService();

		/// <summary>
		/// Downloads and loads the specified asset.
		/// </summary>
		public void DownloadAndLoadAsset(Asset asset, Action<GameObject> callback)
		{
			var assetUri = asset?.ContentUrl;

			if (assetUri == null)
			{
				callback(null);
				return;
			}

			_assetService.DownloadAndLoadAsset(assetUri, asset.ContentType, "Avatar", asset =>
			{
				callback(asset);
			});
		}

		/// <summary>
		/// Downloads and loads the specified asset.
		/// </summary>
		public void DownloadAndLoadAsset(string assetUri, string assetContentType, Action<GameObject> callback)
		{
			DownloadAndLoadAsset(new Asset { ContentUrl = assetUri, ContentType = assetContentType }, callback);
		}
	}
}
