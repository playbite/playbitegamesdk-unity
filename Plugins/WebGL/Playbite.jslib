const plugin = {
    trackPlaybiteEvent: function(eventNameArg, payloadArg) {
        if (!window.Playbite) return;

        var payload = undefined;
        var payloadStr = UTF8ToString(payloadArg);
        if (payloadStr) {
            try {
                payload = JSON.parse(payloadStr);
            } catch (e) {
                console.error(e);
            }
        }

        Playbite.trackEvent(UTF8ToString(eventNameArg), payload);
    },
    onGameComplete: function(payloadArg) {
        if (!window.Playbite) return;

        var payload = undefined;
        var payloadStr = UTF8ToString(payloadArg);
        if (payloadStr) {
            try {
                payload = JSON.parse(payloadStr);
            } catch (e) {
                console.error(e);
            }
        }

        Playbite.postMessage(Object.assign({
            action: 'post-game',
        }, payload));
    },
    openUserProfile: function(displayNameArg) {
        if (!window.Playbite) return;

        Playbite.postMessage({
            action: 'open-profile',
            displayName: UTF8ToString(displayNameArg),
        });
    },
    getPlaybiteAvatar: function() {
        if (!window.Playbite) return null;

        var str = Playbite.getAvatar();

        if (str === null || str === undefined) return null;

        var length = lengthBytesUTF8(str) + 1;
        var buffer = _malloc(length);
        stringToUTF8(str, buffer, length);
        return buffer;
    },
    getPlaybiteUser: function() {
        if (!window.Playbite) return null;
        
        var str = Playbite.getUser();

        if (str === null || str === undefined) return null;

        var length = lengthBytesUTF8(str) + 1;
        var buffer = _malloc(length);
        stringToUTF8(str, buffer, length);
        return buffer;
    },
    getPlaybiteGameData: function() {
        if (!window.Playbite) return null;
        
        var str = Playbite.getGameData();

        if (str === null || str === undefined) return null;

        var length = lengthBytesUTF8(str) + 1;
        var buffer = _malloc(length);
        stringToUTF8(str, buffer, length);
        return buffer;
    },
    getPlaybiteSettings: function() {
        if (!window.Playbite) return null;
        
        var str = Playbite.getSettings();

        if (str === null || str === undefined) return null;

        var length = lengthBytesUTF8(str) + 1;
        var buffer = _malloc(length);
        stringToUTF8(str, buffer, length);
        return buffer;
    },
    getPlaybiteBots: function() {
        if (!window.Playbite) return null;
        
        var str = Playbite.getBots();

        if (str === null || str === undefined) return null;

        var length = lengthBytesUTF8(str) + 1;
        var buffer = _malloc(length);
        stringToUTF8(str, buffer, length);
        return buffer;
    },
    showReactions: function() {
        if (!window.Playbite) return;

        Playbite.postMessage({
            action: 'show-reactions',
        });
    },
    hideReactions: function() {
        if (!window.Playbite) return;

        Playbite.postMessage({
            action: 'hide-reactions',
        });
    },
};

mergeInto(LibraryManager.library, plugin);