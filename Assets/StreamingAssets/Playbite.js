window.Playbite = {
    postMessage: function(payload) {
        var payloadStr = JSON.stringify(payload);

        if (window.ReactNativeWebView) {
            window.ReactNativeWebView.postMessage(payloadStr);
        } else if (window.parent) {
            window.parent.postMessage(payloadStr, '*');
        } else {
            window.postMessage(payloadStr, '*');
        }
    },
    trackEvent: function(eventName, payload) {
        this.postMessage({
            action: 'event',
            eventName,
            payload,
        });
    },
    getAvatar: function() {
        return this._avatar;
    },
    setAvatar: function(value) {
        this._avatar = value;
        sendMessageToPlaybite('{ "action": "data-changed", "dataType": "avatar" }');
    },
    getUser: function() {
        return this._user;
    },
    setUser: function(value) {
        this._user = value;
        sendMessageToPlaybite('{ "action": "data-changed", "dataType": "user" }');
    },
    mute: function() {
        sendMessageToPlaybite('{ "action": "mute" }');
    },
    unmute: function() {
        sendMessageToPlaybite('{ "action": "unmute" }');
    },
    restartGame: function() {
        sendMessageToPlaybite('{ "action": "restart-game" }');
    },
    getGameData: function() {
        return this._gameData;
    },
    setGameData: function(value) {
        this._gameData = value;
        sendMessageToPlaybite('{ "action": "data-changed", "dataType": "game-data" }');
    },
    sendReaction: function(from, emoji) {
        sendMessageToPlaybite(`{ "action": "send-reaction", "from": "${from}", "emoji": "${emoji}" }`);
    },
    getSettings: function() {
        return this._settings;
    },
    setSettings: function(value) {
        this._settings = value;
        sendMessageToPlaybite('{ "action": "data-changed", "dataType": "settings" }');
    },
    getBots: function() {
        return this._bots;
    },
    setBots: function(value) {
        this._bots = value;
    },
};

window.addEventListener("message", (event) => {
    try {
        const data = JSON.parse(event.data);

        if (data.action === 'set-avatar') {
            Playbite.setAvatar(JSON.stringify(data.avatar));
        } else if (data.action === 'set-user') {
            Playbite.setUser(JSON.stringify(data.user));
        } else if (data.action === 'mute') {
            Playbite.mute();
        } else if (data.action === 'unmute') {
            Playbite.unmute();
        } else if (data.action === 'restart-game') {
            Playbite.restartGame();
        } else if (data.action === 'set-game-data') {
            Playbite.setGameData(JSON.stringify(data.data));
        } else if (data.action === 'send-reaction') {
            Playbite.sendReaction(data.from, data.emoji);
        } else if (data.action === 'set-settings') {
            Playbite.setSettings(JSON.stringify(data.settings));
        } else if (data.action === 'set-bots') {
            Playbite.setBots(JSON.stringify(data.bots));
        }
    } catch(e) { 
        console.log(e);
    }
}, false);

window.addEventListener("load", () => {
    if (window.Playbite)
        Playbite.trackEvent('game-ready');
});