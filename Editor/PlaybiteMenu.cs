using UnityEditor;
using UnityEngine;
using Playbite.Setup;
using Playbite.Helpers;
#if PLAYBITE_NET
using Playbite.Components.Networking;
#endif
namespace Playbite.Editor
{
  /// <summary>
	/// Creates menu items for Playbite.
  /// </summary>
  public static class PlaybiteMenu
  {
    /// <summary>
		/// Selects the Settings object.
		/// </summary>
    [MenuItem("Playbite/Settings")]
    public static void OpenSettings()
    {
      Selection.activeObject = PlaybiteSettings.Default;
    }

    /// <summary>
		/// Creates the Platform Controller object in the current scene.
		/// </summary>
    [MenuItem("Playbite/Create Controller")]
    public static void CreatePlaybiteController()
		{
      var prefab = AssetDatabase.LoadAssetAtPath<PlaybiteController>("Packages/com.playbite.sdk/Assets/Prefabs/Playbite.prefab");

      if (prefab == null)
      {
        throw new System.InvalidOperationException("Could not find the Playbite Controller prefab!");
      }

      var instance = PrefabUtility.InstantiatePrefab(prefab) as PlaybiteController;

      if (instance == null)
			{
        throw new System.InvalidOperationException("Could not instantiate Playbite Controller!");
			}

      instance.gameObject.name = "Playbite";

      Undo.RegisterCreatedObjectUndo(instance.gameObject, "Create " + instance.gameObject.name);
      Selection.activeObject = instance.gameObject;
    }

    /// <summary>
		/// Triggers a restart game event coming from the app.
		/// </summary>
    [MenuItem("Playbite/App/Restart")]
    public static void Restart()
    {
      SendAppMessage(new PlaybiteMessage
      {
        Action = "restart-game"
      });
    }

    /// <summary>
		/// Triggers a mute event coming from the app.
		/// </summary>
    [MenuItem("Playbite/App/Mute")]
    public static void Mute()
    {
      SendAppMessage(new PlaybiteMessage
      {
        Action = "mute"
      });
    }

    /// <summary>
		/// Triggers an unmute event coming from the app.
		/// </summary>
    [MenuItem("Playbite/App/Unmute")]
    public static void Unmute()
    {
      SendAppMessage(new PlaybiteMessage
      {
        Action = "unmute"
      });
    }

    /// <summary>
		/// Triggers a pause event coming from the app.
		/// </summary>
    [MenuItem("Playbite/App/Pause")]
    public static void Pause()
    {
      SendAppMessage(new PlaybiteMessage
      {
        Action = "pause"
      });
    }

    /// <summary>
		/// Triggers an unpause event coming from the app.
		/// </summary>
    [MenuItem("Playbite/App/Unpause")]
    public static void Unpause()
    {
      SendAppMessage(new PlaybiteMessage
      {
        Action = "unpause"
      });
    }

    [MenuItem("Playbite/App/Mute", true)]
    [MenuItem("Playbite/App/Unmute", true)]
    [MenuItem("Playbite/App/Pause", true)]
    [MenuItem("Playbite/App/Unpause", true)]
    [MenuItem("Playbite/App/Restart", true)]
    public static bool ValidateRuntmeItems()
    {
      return Application.isPlaying && !!PlaybiteController.Instance;
    }

    private static void SendAppMessage(PlaybiteMessage message)
    {
      if (!PlaybiteController.Instance)
      {
        throw new System.InvalidOperationException("Could not find Playbite Controller!");
      }

      PlaybiteController.Instance.OnMessage(SerializationHelper.ToJson(message));
    }

#if PLAYBITE_NET
    /// <summary>
    /// Creates a Launcher object in the current scene.
    /// </summary>
    [MenuItem("Playbite/Networking/Create Launcher", priority = -3)]
    public static void CreateNetworkLauncher()
    {
      var prefab = AssetDatabase.LoadAssetAtPath<PlaybiteLauncher>("Packages/com.playbite.sdk/Assets/Prefabs/PlaybiteLauncher.prefab");

      if (prefab == null)
      {
        throw new System.InvalidOperationException("Could not find the Playbite Launcher prefab!");
      }

      var instance = PrefabUtility.InstantiatePrefab(prefab) as PlaybiteLauncher;

      if (instance == null)
      {
        throw new System.InvalidOperationException("Could not instantiate Playbite Game Room!");
      }

      instance.gameObject.name = "Playbite Launcher";

      Undo.RegisterCreatedObjectUndo(instance.gameObject, "Create " + instance.gameObject.name);
      Selection.activeObject = instance.gameObject;
    }

    /// <summary>
    /// Creates a basic Room Controller object in the current scene.
    /// </summary>
    [MenuItem("Playbite/Networking/Create Room Controller", priority = -3)]
    public static void CreateNetworkRoomController()
    {
      var prefab = AssetDatabase.LoadAssetAtPath<GameObject>("Packages/com.playbite.sdk/Assets/Prefabs/Playbite Room.prefab");

      if (prefab == null)
      {
        throw new System.InvalidOperationException("Could not find the Playbite Room prefab!");
      }

      var instance = PrefabUtility.InstantiatePrefab(prefab) as GameObject;

      if (instance == null)
      {
        throw new System.InvalidOperationException("Could not instantiate Playbite Room!");
      }

      instance.gameObject.name = "Playbite Room";

      Undo.RegisterCreatedObjectUndo(instance.gameObject, "Create " + instance.gameObject.name);
      Selection.activeObject = instance.gameObject;
    }

    /// <summary>
    /// Creates a game Room Controller object in the current scene.
    /// </summary>
    [MenuItem("Playbite/Networking/Create Game Room Controller", priority = -3)]
    public static void CreateNetworkGameRoomController()
    {
      var prefab = AssetDatabase.LoadAssetAtPath<GameObject>("Packages/com.playbite.sdk/Assets/Prefabs/Playbite Game Room.prefab");

      if (prefab == null)
      {
        throw new System.InvalidOperationException("Could not find the Playbite Game Room prefab!");
      }

      var instance = PrefabUtility.InstantiatePrefab(prefab) as GameObject;

      if (instance == null)
      {
        throw new System.InvalidOperationException("Could not instantiate Playbite Game Room!");
      }

      instance.gameObject.name = "Playbite Game Room";

      Undo.RegisterCreatedObjectUndo(instance.gameObject, "Create " + instance.gameObject.name);
      Selection.activeObject = instance.gameObject;
    }
#endif
  }
}