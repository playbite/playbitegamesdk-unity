﻿#if PLAYBITE && UNITY_EDITOR
using UnityEngine;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor;
using System.Linq;
using System;
using System.IO;
using Playbite.Setup;
#if PLAYBITE_NET
using Playbite.Components.Networking;
#endif

namespace Playbite.Editor
{
	/// <summary>
	/// Editor utility for Playbite.
	/// </summary>
	[InitializeOnLoad]
	public static class PlaybiteEditorUtility
	{
    static PlaybiteEditorUtility()
    {
      EnsureDependenciesSetUp();
    }

    [UnityEditor.Callbacks.DidReloadScripts]
    private static void EnsureDependenciesSetUp()
    {
      RetryEnsureNetworkingDependenciesSetUp();
    }

    internal static void RetryEnsureNetworkingDependenciesSetUp()
    {
      if (EditorApplication.isCompiling || EditorApplication.isUpdating)
      {
        EditorApplication.delayCall += RetryEnsureNetworkingDependenciesSetUp;
        return;
      }
      EditorApplication.delayCall += EnsureNetworkingDependenciesSetUp;
    }

    static void EnsureNetworkingDependenciesSetUp()
    {
#if PLAYBITE_NET
      if (!File.Exists("Assets/Photon/Fusion/Resources/PhotonAppSettings.asset"))
      {
        var defaultRoomPrefab = AssetDatabase.LoadAssetAtPath<PlaybiteRoomController>("Packages/com.playbite.sdk/Assets/Prefabs/PlaybiteGameRoom.prefab");
        PlaybiteSettings.Default.MultiplayerSettings.RoomPrefab = defaultRoomPrefab;
        EditorUtility.SetDirty(PlaybiteSettings.Default);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        AssetDatabase.ImportPackage("Packages/com.playbite.sdk/Assets/Packages/photon-fusion.unitypackage", false);
      }

      var symbolsString = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.WebGL);
      if (!symbolsString.Contains("PLAYBITE_NET_FUSION"))
      {
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.WebGL,
          string.Join(';', symbolsString.Split(';').Concat(new[] { "PLAYBITE_NET_FUSION" })));
      }

#if PLAYBITE_NET_FUSION
      if (Fusion.NetworkProjectConfig.Global.AssembliesToWeave?.Any(x => x == "Playbite") != true)
      {
        Fusion.NetworkProjectConfig.Global.AssembliesToWeave =
          (Fusion.NetworkProjectConfig.Global.AssembliesToWeave ?? Enumerable.Empty<string>())
            .Concat(new[] { "Playbite" }).ToArray();
        Fusion.Editor.NetworkProjectConfigUtilities.SaveGlobalConfig();
      }

      if (string.IsNullOrEmpty(Fusion.Photon.Realtime.PhotonAppSettings.Instance.AppSettings.AppIdFusion))
      {
        Fusion.Photon.Realtime.PhotonAppSettings.Instance.AppSettings.AppIdFusion = "b460615a-8f32-4b15-801b-d49940e79793";
        Fusion.Photon.Realtime.PhotonAppSettings.Instance.AppSettings.FixedRegion = "us";
        EditorUtility.SetDirty(Fusion.Photon.Realtime.PhotonAppSettings.Instance);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        // Causing build errors in some games
        //EditorWindow.GetWindow<Fusion.Editor.FusionHubWindow>().Close();
      }
#endif
#else
      var symbolsString = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.WebGL);
      if (symbolsString.Contains("PLAYBITE_NET_FUSION"))
      {
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.WebGL,
          string.Join(';', symbolsString.Split(';').Except(new[] { "PLAYBITE_NET_FUSION" })));
      }
#endif
    }
  }
}
#endif