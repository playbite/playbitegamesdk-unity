﻿using UnityEngine;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor;
using System.IO;

#if PLAYBITE && UNITY_WEBGL
namespace Playbite.Editor
{
	/// <summary>
	/// Pre processor to copy streaming assets needed.
	/// </summary>
	public class StreamingAssetsPreProcessor : IPreprocessBuildWithReport
	{
		public int callbackOrder => -1001;

		public void OnPreprocessBuild(BuildReport report)
		{
			Debug.Log("[Playbite] Starting to pre-process streaming assets...");

			var destinationFolder = Path.GetFullPath("Assets/StreamingAssets/Playbite");
			var sourceFolder = Path.GetFullPath("Packages/com.playbite.sdk/Assets/StreamingAssets");

			Debug.Log($"[Playbite] Preparing to move streaming asset files to {destinationFolder}...");
			Debug.Log($"[Playbite] Copying streaming assets from {sourceFolder}...");

			Directory.CreateDirectory(destinationFolder);
			FileUtil.ReplaceDirectory(sourceFolder, destinationFolder);

			AssetDatabase.Refresh();

			Debug.Log("[Playbite] Done pre-processing streaming assets...");
		}
	}
}
#endif