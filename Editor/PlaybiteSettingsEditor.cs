using System.Collections.Generic;
using System.IO;
using System.Linq;
using Playbite.Setup;
using UnityEditor;
using UnityEngine;

namespace Playbite.Editor
{
  /// <summary>
  /// The editor for the Playbite Settings.
  /// </summary>
  [CustomEditor(typeof(PlaybiteSettings), true)]
  public class PlaybiteSettingsEditor : UnityEditor.Editor
  {
    private SerializedProperty _gameIdProp;
    private SerializedProperty _enableDefaultAvatarInEditorProp;
    private SerializedProperty _enableDefaultAvatarInPlayerProp;
    private SerializedProperty _defaultAvatarProp;
    private SerializedProperty _defaultExtraAssetsProp;
    private SerializedProperty _gameConfigProp;
    private SerializedProperty _enableDefaultGameSettingsProp;
    private SerializedProperty _defaultGameSettingsProp;
    private SerializedProperty _enableDefaultGameDataProp;
    private SerializedProperty _defaultGameDataProp;
    private SerializedProperty _multiplayerSettingsProp;

    public void OnEnable()
    {
      _gameIdProp = serializedObject.FindProperty(nameof(PlaybiteSettings.GameId));
      _enableDefaultAvatarInEditorProp = serializedObject.FindProperty(nameof(PlaybiteSettings.EnableDefaultAvatarInEditor));
      _enableDefaultAvatarInPlayerProp = serializedObject.FindProperty(nameof(PlaybiteSettings.EnableDefaultAvatarInPlayer));
      _defaultAvatarProp = serializedObject.FindProperty(nameof(PlaybiteSettings.DefaultAvatar));
      _defaultExtraAssetsProp = serializedObject.FindProperty(nameof(PlaybiteSettings.DefaultExtraAssets));
      _gameConfigProp = serializedObject.FindProperty(nameof(PlaybiteSettings.GameConfig));
      _enableDefaultGameSettingsProp = serializedObject.FindProperty(nameof(PlaybiteSettings.EnableDefaultGameSettings));
      _defaultGameSettingsProp = serializedObject.FindProperty(nameof(PlaybiteSettings.DefaultGameSettings));
      _enableDefaultGameDataProp = serializedObject.FindProperty(nameof(PlaybiteSettings.EnableDefaultGameData));
      _defaultGameDataProp = serializedObject.FindProperty(nameof(PlaybiteSettings.DefaultGameData));
      _multiplayerSettingsProp = serializedObject.FindProperty(nameof(PlaybiteSettings.MultiplayerSettings));
    }

    public override void OnInspectorGUI()
    {
      DrawHeader();

      DrawPlatformSetup();

      serializedObject.ApplyModifiedProperties();
    }

    private new void DrawHeader()
    {
      GUILayout.BeginHorizontal();

      GUILayout.BeginVertical();

      GUILayout.Space(8);

      GUILayout.BeginHorizontal();

      GUILayout.Label("Playbite Game Setup", EditorStyles.boldLabel);

      GUILayout.EndHorizontal();
      GUILayout.EndVertical();

      GUILayout.EndHorizontal();

      EditorGUILayout.Space();

      EditorGUILayout.PropertyField(_gameIdProp);

      if (string.IsNullOrWhiteSpace(_gameIdProp.stringValue))
      {
        EditorGUILayout.HelpBox("A unique game id is required to use the Playbite SDK.", MessageType.Warning);
      }

      EditorGUILayout.Space();
    }

    private void DrawPlatformSetup()
    {
      GUILayout.Label("Features:");

      EditorGUILayout.Space();

      DrawTargetSettings(BuildTargetGroup.WebGL);

      EditorGUILayout.Space();
    }

    private void DrawTargetSettings(BuildTargetGroup target)
    {
      var symbolsString = PlayerSettings.GetScriptingDefineSymbolsForGroup(target);
      var symbols = symbolsString.Split(';').ToList();

      DrawDefineSymbolToggle("PLAYBITE", "Enable Playbite integration", ref symbols);
      DrawPlaybiteSettings();

      DrawDefineSymbolToggle("PLAYBITE_AVATARS", "Enable Playbite avatars", ref symbols);
      DrawAvatarSettings();

      DrawDefineSymbolToggle("PLAYBITE_NET", "Enable Playbite networking", ref symbols);
      DrawNetworkSettings();

      var newSymbolsString = string.Join(";", symbols.ToArray());
      if (newSymbolsString != symbolsString)
      {
        PlayerSettings.SetScriptingDefineSymbolsForGroup(target, newSymbolsString);
      }
    }

    private bool DrawDefineSymbolToggle(string symbol, string text, ref List<string> symbols)
    {
      var contains = symbols.Contains(symbol);
      var changed = false;

      if (GUILayout.Toggle(contains, text) != contains)
      {
        changed = true;
        if (contains)
        {
          symbols.Remove(symbol);
          if (symbol == "PLAYBITE_NET")
          {
            symbols.Remove("PLAYBITE_NET_FUSION");
          }
        }
        else
        {
          symbols.Add(symbol);
        }
      }

      EditorGUILayout.Space();

      return changed;
    }

    private bool IsCapabilityEnabled(string capability)
    {
      var symbolsString = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.WebGL);
      var symbols = symbolsString.Split(';').ToList();

      return symbols.Contains(capability);
    }

    private void DrawPlaybiteSettings()
    {
      if (!IsCapabilityEnabled("PLAYBITE"))
        return;

      GUILayout.Label("Game Settings:");

      EditorGUILayout.BeginHorizontal();

      EditorGUILayout.Space(0.5f);

      EditorGUILayout.BeginVertical();
      EditorGUILayout.Space();
      EditorGUILayout.PropertyField(_gameConfigProp, new GUIContent("Config"));
      EditorGUILayout.EndVertical();

      EditorGUILayout.EndHorizontal();

      EditorGUILayout.Space();

      _enableDefaultGameSettingsProp.boolValue =
        GUILayout.Toggle(_enableDefaultGameSettingsProp.boolValue, "Enable default game settings (editor-only)");

      if (_enableDefaultGameSettingsProp.boolValue)
      {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.Space(0.5f);

        EditorGUILayout.BeginVertical();
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(_defaultGameSettingsProp, new GUIContent("Settings"));
        EditorGUILayout.EndVertical();

        EditorGUILayout.EndHorizontal();
      }

      GUILayout.Label("Game Data:");

      EditorGUILayout.Space();

      _enableDefaultGameDataProp.boolValue =
        GUILayout.Toggle(_enableDefaultGameDataProp.boolValue, "Enable default game data");

      if (_enableDefaultGameDataProp.boolValue)
      {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.Space(0.5f);

        EditorGUILayout.BeginVertical();
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(_defaultGameDataProp, new GUIContent("Data (JSON)"), GUILayout.Height(100));
        EditorGUILayout.EndVertical();

        EditorGUILayout.EndHorizontal();
      }

      EditorGUILayout.Space();
    }

    private void DrawAvatarSettings()
    {
      if (!IsCapabilityEnabled("PLAYBITE_AVATARS"))
        return;

      GUILayout.Label("Assets:");

      EditorGUILayout.Space();

      _enableDefaultAvatarInEditorProp.boolValue =
        GUILayout.Toggle(_enableDefaultAvatarInEditorProp.boolValue, "Enable default assets");

      if (_enableDefaultAvatarInEditorProp.boolValue)
      {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.Space(0.5f);

        EditorGUILayout.BeginVertical();
        EditorGUILayout.Space();

        _enableDefaultAvatarInPlayerProp.boolValue =
          GUILayout.Toggle(_enableDefaultAvatarInPlayerProp.boolValue, "Also enable in final build");

        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(_defaultAvatarProp, new GUIContent("Avatar"));

        EditorGUILayout.PropertyField(_defaultExtraAssetsProp, new GUIContent("Other Assets"));

        EditorGUILayout.EndVertical();

        EditorGUILayout.EndHorizontal();
      }

      EditorGUILayout.Space();
    }

    private void DrawNetworkSettings()
    {
      var symbolsString = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.WebGL);
      var symbols = symbolsString.Split(';').ToList();

      if (!symbols.Contains("PLAYBITE_NET"))
        return;

      EditorGUILayout.BeginHorizontal();
      EditorGUILayout.Space(0.5f);

      EditorGUILayout.BeginVertical();
      EditorGUILayout.Space();
      EditorGUILayout.PropertyField(_multiplayerSettingsProp, new GUIContent("Multiplayer Settings"));
      EditorGUILayout.Space();
      EditorGUILayout.EndVertical();

      EditorGUILayout.EndHorizontal();
    }

    private static void InstallNetworkingDependencies()
    {
    }
  }
}