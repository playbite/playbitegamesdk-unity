﻿using UnityEngine;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor;
using System.IO;

#if PLAYBITE && UNITY_WEBGL
namespace Playbite.Editor
{
	/// <summary>
	/// Pre processor to set the right web gl template for a Playbite game.
	/// </summary>
	public class WebGLTemplatePreProcessor : IPreprocessBuildWithReport
	{
		public int callbackOrder => -1000;

		public void OnPreprocessBuild(BuildReport report)
		{
			Debug.Log("[Playbite] Starting to pre-process webl template...");

			var destinationFolder = Path.GetFullPath("Assets/WebGLTemplates/Playbite");
			var sourceFolder = Path.GetFullPath("Packages/com.playbite.sdk/Assets/WebGLTemplates/Playbite");

			Debug.Log($"[Playbite] Preparing to move template file to {destinationFolder}...");
			Debug.Log($"[Playbite] Copying template from {sourceFolder}...");

			Directory.CreateDirectory(destinationFolder);
			FileUtil.ReplaceDirectory(sourceFolder, destinationFolder);

			AssetDatabase.Refresh();

			Debug.Log($"[Playbite] Setting webgl template, old was = {PlayerSettings.WebGL.template}");

			PlayerSettings.WebGL.template = "PROJECT:Playbite";

			Debug.Log($"[Playbite] Set webgl template to {PlayerSettings.WebGL.template}");

			Debug.Log("[Playbite] Done pre-processing webl template...");
		}
	}
}
#endif