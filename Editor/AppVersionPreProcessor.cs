﻿#if PLAYBITE
using UnityEngine;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor;
using System.Linq;
using System;

namespace Playbite.Editor
{
	/// <summary>
	/// Pre processor that sets a unique build version.
	/// </summary>
	public class AppVersionPreProcessor : IPreprocessBuildWithReport
	{
		public int callbackOrder => 1;

		public void OnPreprocessBuild(BuildReport report)
		{
			Debug.Log($"[Playbite] Starting to pre-process build version {PlayerSettings.bundleVersion}...");

			var versionParts = PlayerSettings.bundleVersion.Split('.').ToList();
			if (versionParts.Count < 2)
			{
				Debug.LogError("BuildPostprocessor failed to update version " + PlayerSettings.bundleVersion);
				return;
			}

			var buildVersion = DateTime.UtcNow.ToString("yyyymmssfff");

			if (versionParts.Count == 2)
				versionParts.Add(buildVersion);
			else
				versionParts[2] = buildVersion;

			PlayerSettings.bundleVersion = string.Join('.', versionParts);

			Debug.Log($"[Playbite] Done pre-processing build version to {PlayerSettings.bundleVersion}.");
		}
	}
}
#endif