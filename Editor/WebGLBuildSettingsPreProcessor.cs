﻿#if PLAYBITE && UNITY_WEBGL
using UnityEngine;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor;
using UnityEditor.Rendering;
using UnityEngine.Rendering;
using Playbite.Setup;

namespace Playbite.Editor
{
  /// <summary>
  /// Pre processor to set the right web gl template for a Playbite game.
  /// </summary>
  public class WebGLBuildSettingsPreProcessor : IPreprocessBuildWithReport
  {
    public int callbackOrder => -100;

    public void OnPreprocessBuild(BuildReport report)
    {
      Debug.Log("[Playbite] Starting to pre-process webl build settings...");

      PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;
      PlayerSettings.WebGL.decompressionFallback = false;
      PlayerSettings.WebGL.dataCaching = false;
      PlayerSettings.WebGL.nameFilesAsHashes = true;
      PlayerSettings.runInBackground = true;
#if PLAYBITE_NET
      PlayerSettings.SetManagedStrippingLevel(BuildTargetGroup.WebGL, ManagedStrippingLevel.Minimal);
#else
      PlayerSettings.SetManagedStrippingLevel(BuildTargetGroup.WebGL, ManagedStrippingLevel.High);
#endif
      PlayerSettings.SplashScreen.show = false;
      EditorUserBuildSettings.SetPlatformSettings(BuildPipeline.GetBuildTargetName(BuildTarget.WebGL), "CodeOptimization", "size");

      if (EditorUserBuildSettings.development)
      {
        PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.FullWithStacktrace;
      }
      else
      {
        PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
      }

      var tierSettings = new TierSettings
      {
        cascadedShadowMaps = false,
        detailNormalMap = false,
        enableLPPV = false,
        hdr = false,
        hdrMode = CameraHDRMode.R11G11B10,
        prefer32BitShadowMaps = false,
        realtimeGICPUUsage = RealtimeGICPUUsage.Low,
        reflectionProbeBlending = false,
        reflectionProbeBoxProjection = false,
        renderingPath = RenderingPath.Forward,
        semitransparentShadows = false,
        standardShaderQuality = ShaderQuality.Low
      };
      EditorGraphicsSettings.SetTierSettings(BuildTargetGroup.WebGL, GraphicsTier.Tier1, tierSettings);
      EditorGraphicsSettings.SetTierSettings(BuildTargetGroup.WebGL, GraphicsTier.Tier2, tierSettings);
      EditorGraphicsSettings.SetTierSettings(BuildTargetGroup.WebGL, GraphicsTier.Tier3, tierSettings);

      Debug.Log("[Playbite] Done pre-processing webl build settings...");
    }
  }
}
#endif