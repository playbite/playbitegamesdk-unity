﻿#if PLAYBITE && UNITY_WEBGL
using UnityEngine;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor;
using UnityEngine.Rendering;

namespace Playbite.Editor
{
  /// <summary>
  /// Pre processor to ensure the right shaders are included in the build.
  /// </summary>
  public class PlaybiteShaderPreProcessor : IPreprocessBuildWithReport
  {
    public int callbackOrder => 1;

    public void OnPreprocessBuild(BuildReport report)
    {
      Debug.Log("[Playbite] Starting to pre-process shader settings...");

#if PLAYBITE_AVATARS
      //AddAlwaysIncludedShader("glTF/Unlit");
      //AddAlwaysIncludedShader("glTF/PbrSpecularGlossiness");
      //AddAlwaysIncludedShader("glTF/PbrMetallicRoughness");
#endif

      Debug.Log("[Playbite] Done pre-processing shader settings...");
    }

    private static void AddAlwaysIncludedShader(string shaderName)
    {
      Debug.Log($"[Playbite] Adding shader to be always included: {shaderName}");

      var shader = Shader.Find(shaderName);
      if (shader == null)
      {
        Debug.Log("[Playbite] Shader not found.");
        return;
      }

      var graphicsSettingsObj = AssetDatabase.LoadAssetAtPath<GraphicsSettings>("ProjectSettings/GraphicsSettings.asset");
      var serializedObject = new SerializedObject(graphicsSettingsObj);
      var arrayProp = serializedObject.FindProperty("m_AlwaysIncludedShaders");
      bool hasShader = false;
      for (int i = 0; i < arrayProp.arraySize; ++i)
      {
        var arrayElem = arrayProp.GetArrayElementAtIndex(i);
        if (shader == arrayElem.objectReferenceValue)
        {
          hasShader = true;
          break;
        }
      }

      if (!hasShader)
      {
        int arrayIndex = arrayProp.arraySize;
        arrayProp.InsertArrayElementAtIndex(arrayIndex);
        var arrayElem = arrayProp.GetArrayElementAtIndex(arrayIndex);
        arrayElem.objectReferenceValue = shader;

        serializedObject.ApplyModifiedProperties();

        AssetDatabase.SaveAssets();

        Debug.Log("[Playbite] Shader added successfully.");
      }
      else
      {
        Debug.Log("[Playbite] Shader was already present, skipped.");
      }
    }
  }
}
#endif